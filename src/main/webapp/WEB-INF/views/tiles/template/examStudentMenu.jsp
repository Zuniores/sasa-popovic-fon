<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>


<ul class="menu">
	<li>
		<c:url value="/examStudent/add" var="examStudentAdd"></c:url>
		<a href="<c:out value="${examStudentAdd}"/>">Add exam application</a>
	</li>
	<li>
		<c:url value="/examStudent/list" var="examStudentList"></c:url>
		<a href="<c:out value="${examStudentList}"/>">List exam applications</a>
	</li>
</ul>


