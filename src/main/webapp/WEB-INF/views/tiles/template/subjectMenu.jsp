<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>


<ul class="menu">
	<li>
		<c:url value="/subject/add" var="subjectAdd"></c:url>
		<a href="<c:out value="${subjectAdd}"/>">Add subject</a>
	</li>
	<li>
		<c:url value="/subject/list" var="subjectList"></c:url>
		<a href="<c:out value="${subjectList}"/>">List subjects</a>
	</li>
</ul>


