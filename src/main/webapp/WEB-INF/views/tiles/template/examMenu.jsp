<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>


<ul class="menu">
	<li>
		<c:url value="/exam/add" var="examAdd"></c:url>
		<a href="<c:out value="${examAdd}"/>">Add exam</a>
	</li>
	<li>
		<c:url value="/exam/list" var="examList"></c:url>
		<a href="<c:out value="${examList}"/>">List exams</a>
	</li>
</ul>


