<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>


<style>
#logout{
    border-bottom: dashed;
    border-bottom-color: white;
    border-right: dashed;
    border-right-color: white;
}
</style>

<ul class="menu">
	<li id="logout">
		<c:url value="/authentication/logout" var="logout"></c:url>
		<a href="<c:out value="${logout}"/>">Logout</a>
	</li>
	<li>
		<c:url value="/professor" var="professorHome"></c:url>
		<a href="<c:out value="${professorHome}"/>">Professors</a>
	</li>
	<li>
		<c:url value="/student" var="studentHome"></c:url>
		<a href="<c:out value="${studentHome}"/>">Students</a>
	</li>
	<li>
		<c:url value="/exam" var="examHome"></c:url>
		<a href="<c:out value="${examHome}"/>">Exams</a>
	</li>
	<li>
		<c:url value="/examStudent" var="examStudentHome"></c:url>
		<a href="<c:out value="${examStudentHome}"/>">Exam applications</a>
	</li>
	<li>
		<c:url value="/subject" var="subjectHome"></c:url>
		<a href="<c:out value="${subjectHome}"/>">Subjects</a>
	</li>
	<li>
		<c:url value="/city" var="cityHome"></c:url>
		<a href="<c:out value="${cityHome}"/>">Cities</a>
	</li>
</ul>

