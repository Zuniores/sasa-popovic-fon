<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>


<ul class="menu">
	<li>
		<c:url value="/city/add" var="cityAdd"></c:url>
		<a href="<c:out value="${cityAdd}"/>">Add city</a>
	</li>
	<li>
		<c:url value="/city/list" var="cityList"></c:url>
		<a href="<c:out value="${cityList}"/>">List cities</a>
	</li>
</ul>


