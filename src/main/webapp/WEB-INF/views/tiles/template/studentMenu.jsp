<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>


<ul class="menu">
	<li>
		<c:url value="/student/add" var="studentAdd"></c:url>
		<a href="<c:out value="${studentAdd}"/>">Add student</a>
	</li>
	<li>
		<c:url value="/student/list" var="studentList"></c:url>
		<a href="<c:out value="${studentList}"/>">List students</a>
	</li>
</ul>


