<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>


<ul class="menu">
	<li>
		<c:url value="/professor/add" var="professorAdd"></c:url>
		<a href="<c:out value="${professorAdd}"/>">Add professor</a>
	</li>
	<li>
		<c:url value="/professor/list" var="professorList"></c:url>
		<a href="<c:out value="${professorList}"/>">List professors</a>
	</li>
</ul>


