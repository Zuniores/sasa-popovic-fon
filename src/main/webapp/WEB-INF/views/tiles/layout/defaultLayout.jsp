<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title><tiles:getAsString name="title"></tiles:getAsString></title>
<!-- <link rel="stylesheet" type="text/css" href="../resources/res/css/Main.css" /> -->
<!-- <link rel="stylesheet" type="text/css" href="../resources/res/css/Style.css" /> -->
<!-- <link rel="stylesheet" type="text/css" href="../resources/res/css/fonts/fontawesome/css/all.min.css" /> -->
<style type="text/css">
/* @import "../src/main/webapp/resources/res/css/Style.css"; */

/* @import "../src/main/webapp/resources/res/css/fonts/fontawesome/css/all.min.css"; */
<%@include file="/resources/res/css/Style.css" %>
<%@include file="/resources/res/css/fonts/fontawesome/css/all.min.css" %>
<%@include file="/resources/res/css/Main.css" %>
<%@include file="/resources/res/css/Layout.css" %>
body {
/* 	padding-top: 56px; */
    display: flex;
    flex-direction: column;
    height: 100vh;
    font-family: Arial;
}
.menu{
	font-size: 1.2em;
	font-family: Arial;
	line-height: 1.5;
}

#site-content{
	width: 100%;
}
.fill{
flex: 1;
padding: 0;
min-height: 100vh;
}
.flex-container {
    height: auto;
}

.menu>li{
	white-space: nowrap;
}

#levo {
	flex-grow: 1;
}

#desno {
	flex-grow: 3;
}

footer a {
	color: black;
}

footer a:visited {
	color: green;
}
@media (min-width: 768px)
.col-md-8 {
	-webkit-flex: 0 0 100%;
	-moz-flex: 0 0 100%;
    -ms-flex: 0 0 100%;
    flex: 0 0 100%;
    max-width: 100%;
}
</style>
</head>
<body>

	<div class="grid-container">

		<header id="header">
			<tiles:insertAttribute name="header" />
		</header>

		<aside id="levo">
			<section id="sidemenu">
				<tiles:insertAttribute name="menu" />
			</section>
		</aside>

		<main id="desno">
			<div class="flex-container">
				<section id="site-content">
					<tiles:insertAttribute name="body" />
				</section>
			</div>
		</main>

		<footer id="footer">
			<tiles:insertAttribute name="footer" />
		</footer>
	</div>
	
	

</body>
</html>