<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<!-- Bootstrap core CSS -->
<!-- <link href="resources/bootstrap/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
<!-- <link href="resources/jquery/jquery-ui-1.12.1/jquery-ui-1.12.1.custom/jquery-ui.min.css" rel="stylesheet"> -->
<style>
<%@include file="../resources/bootstrap/bootstrap/css/bootstrap.min.css" %>
<%@include file="../resources/jquery/jquery-ui-1.12.1/jquery-ui-1.12.1.custom/jquery-ui.min.css" %>
</style>
</head>

	<c:set value="${pageContext.request.contextPath}" var="contextPath"></c:set>

<body data-gr-c-s-loaded="true">

	<div class="container fill">
		<div class="row">
			<div class="col-md-7">
				<br>
				<div class="card mb-4">
					<div class="card-body">
						<h2 class="card-title">Add professor</h2>
						
						<form:form class="form-horizontal" action="${contextPath}/professor/confirm" method="post"
							modelAttribute="professorDto">
							<fieldset>
<!-- 								<div class="form-group row"> -->
<!-- 									<label class="col-sm-3 col-form-label" for="id">Id: </label> -->
<!-- 									<div class="col-sm-8"> -->
<%-- 										<form:input type="text" class="form-control" path="id" id="id" required="" /> --%>
<!-- 										<div class="invalid-feedback"></div> -->
<%-- 										<form:errors path="id" cssClass="error" /> --%>
<!-- 									</div> -->
<!-- 								</div> -->
								
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="firstname">First Name: </label>
									<div class="col-sm-8">
										<form:input type="text" class="form-control" path="firstname" id="firstname" required="" />
										<div class="invalid-feedback"></div>
										<form:errors path="firstname" cssClass="error" />
									</div>
								</div>
								
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="lastname">Last Name: </label>
									<div class="col-sm-8">
										<form:input type="text" class="form-control" path="lastname" id="lastname" />
										<div class="invalid-feedback"></div>
										<form:errors path="lastname" cssClass="error" />
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="email">Email: </label>
									<div class="col-sm-8">
										<form:input type="text" class="form-control" path="email" id="email" />
										<div class="invalid-feedback"></div>
										<form:errors path="email" cssClass="error" />
										<c:if test="${not empty errorMessage }">
											<div>${errorMessage}</div>
										</c:if>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="address">Address: </label>
									<div class="col-sm-8">
										<form:input type="text" class="form-control" path="address" id="address" />
										<div class="invalid-feedback"></div>
										<form:errors path="address" cssClass="error" />
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="cityDto">City: </label>
									<div class="col-sm-8">
										<form:select id="cityDto" path="cityDto" class="form-control">
											<form:options items="${cityDtos}" itemValue="id" itemLabel="name"/>
										</form:select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="phone">Phone number: </label>
									<div class="col-sm-8">
										<form:input type="text" class="form-control" path="phone" id="phone" />
										<div class="invalid-feedback"></div>
										<form:errors path="phone" cssClass="error" />
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="reelectionDate">Reelection date: </label>
									<div class="col-sm-8">
										<form:input type="date" class="form-control" path="reelectionDate" id="reelectionDate" />
										<div class="invalid-feedback"></div>
										<form:errors path="reelectionDate" cssClass="error" />
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="titleDto">Title: </label>
									<div class="col-sm-8">
										<form:select id="titleDto" path="titleDto" class="form-control">
											<form:options items="${titleDtos}" itemValue="id" itemLabel="name"/>
										</form:select>
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-4">
										<button id="btnSubmitForm" name="submitForm" class="btn btn-primary" type="submit">Confirm</button>
									</div>
								</div>
							</fieldset>
						</form:form>

					</div>
					
				</div>
			</div>

			<div class="col-md-5">
				<!-- Search Widget -->
				<div class="card my-4">
					<h5 class="card-header">Professors</h5>
					<div class="card-body">
						<div class="row">
							<table class="table" id="employees">
								<thead>
									<tr>
										<th scope="col">#</th>
										<th scope="col">First name</th>
										<th scope="col">Last name</th>
										<th scope="col">Email</th>
										<th scope="col">Title</th>
									</tr>
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<footer class="py-5 bg-dark">
		<div class="container">
			<p class="m-0 text-center text-white">Copyright © Your Website 2020</p>
		</div>
	</footer>

<!-- 	<script src="resources/jquery/jquery-3.4.1.min.js"></script> -->
<!-- 	<script src="resources/bootstrap/bootstrap/js/bootstrap.bundle.min.js"></script> -->
<!-- 	<script src="resources/jquery/jquery-ui-1.12.1/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script> -->
	
	<script>
		
		
	</script>
</body>
</html>