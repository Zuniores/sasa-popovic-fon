<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Add student</title>
<!-- Bootstrap core CSS -->
<!-- <link href="resources/bootstrap/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
<!-- <link href="resources/jquery/jquery-ui-1.12.1/jquery-ui-1.12.1.custom/jquery-ui.min.css" rel="stylesheet"> -->
<style>
<%@include file="../resources/bootstrap/bootstrap/css/bootstrap.min.css" %>
<%@include file="../resources/jquery/jquery-ui-1.12.1/jquery-ui-1.12.1.custom/jquery-ui.min.css" %>
</style>
</head>

	<c:set value="${pageContext.request.contextPath}" var="contextPath"></c:set>

<body data-gr-c-s-loaded="true">

	<div class="container fill">
		<div class="row">
			<div class="col-md-7">
				<br>
				<div class="card mb-4">
					<div class="card-body">
						<h2 class="card-title">Edit professor</h2>
						
						<form:form class="form-horizontal" action="${contextPath}/professor/update" method="post" 
						modelAttribute="professorDto">
							<fieldset>
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="id" hidden="true">Id: </label>
									<div class="col-sm-8">
										<form:input type="text" class="form-control" path="id" id="id"  
											value = "${professorDto.getId()}" hidden="true"/>
										<div class="invalid-feedback"></div>
										<form:errors path="id" cssClass="error" />
									</div>
								</div>
								
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="firstname">First Name: </label>
									<div class="col-sm-8">
										<form:input type="text" class="form-control" path="firstname" id="firstname"  
											value="${professorDto.getFirstname()}"/>
										<div class="invalid-feedback"></div>
										<form:errors path="firstname" cssClass="error" />
									</div>
								</div>
								
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="lastname">Last Name: </label>
									<div class="col-sm-8">
										<form:input type="text" class="form-control" path="lastname" id="lastname"  
											value="${professorDto.getLastname()}"/>
										<div class="invalid-feedback"></div>
										<form:errors path="lastname" cssClass="error" />
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="email">Email: </label>
									<div class="col-sm-8">
										<form:input type="text" class="form-control" path="email" id="email"  
											value="${professorDto.getEmail()}"/>
										<div class="invalid-feedback"></div>
										<form:errors path="email" cssClass="error" />
									</div>
								</div>
								<c:if test="${not empty errorMessage }">
									<div>${errorMessage}</div>
								</c:if>
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="address">Address: </label>
									<div class="col-sm-8">
										<form:input type="text" class="form-control" path="address" id="address"  
											value="${professorDto.getAddress()}"/>
										<div class="invalid-feedback"></div>
										<form:errors path="address" cssClass="error" />
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="cityDto">City</label>
									<div class="col-sm-8">
										<form:select id="cityDto" path="cityDto" class="form-control">
											<c:forEach items="${cityDtos}" var="s" varStatus="status">
										        <c:choose>
										            <c:when test="${s.getId().toString() == professorDto.getCityDto().getId().toString()}">
										                <option value="${s.getId()}" label="${s.getName()}" selected="selected" />
										            </c:when>
										            <c:otherwise>
										                <option value="${s.getId()}" label="${s.getName()}"/>
										            </c:otherwise>
										        </c:choose> 
										    </c:forEach>
<%-- 											<form:option value="">${professorDto.getCityDto().getName()}</form:option> --%>
<%-- 											<form:option value="${professorDto.getCityDto().getId()}" label="${professorDto.getCityDto().getName()}" selected="selected" /> --%>
<%-- 											<form:options items="${cityDtos}" itemValue="id" itemLabel="name" /> --%>
										</form:select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="phone">Phone number: </label>
									<div class="col-sm-8">
										<form:input type="text" class="form-control" path="phone" id="phone" required="" 
											value="${professorDto.getPhone()}"/>
										<div class="invalid-feedback"></div>
										<form:errors path="phone" cssClass="error" />
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="reelectionDate">Reelection date: </label>
									<div class="col-sm-8">
										<form:input type="date" class="form-control" path="reelectionDate" id="reelectionDate" required="" 
											value="${professorDto.getReelectionDate()}"/>
										<div class="invalid-feedback"></div>
										<form:errors path="reelectionDate" cssClass="error" />
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="titleDto">Title: </label>
									<div class="col-sm-8">
										<form:select id="cityDto" path="titleDto" class="form-control">
										<form:option value="">${professorDto.getTitleDto().getName()}</form:option>
											<form:options items="${titleDtos}" itemValue="id" itemLabel="name"/>
										</form:select>
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-md-4">
										<button id="btnSubmitForm" name="submitForm" class="btn btn-primary" type="submit">Update</button>
									</div>
								</div>
							</fieldset>
						</form:form>

					</div>
					
				</div>
			</div>
			<div class="col-md-5">
				<!-- Search Widget -->
				<div class="card my-4">
					<h5 class="card-header">Professors</h5>
					<div class="card-body">
						<div class="row">
							<table class="table" id="employees">
								<thead>
									<tr>
										<th scope="col">#</th>
										<th scope="col">First name</th>
										<th scope="col">Last name</th>
										<th scope="col">Email</th>
										<th scope="col">Title</th>
									</tr>
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="resources/jquery/jquery-3.4.1.min.js"></script>
	<script src="resources/bootstrap/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="resources/jquery/jquery-ui-1.12.1/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
	
	<script>
		
		
	</script>
</body>
</html>