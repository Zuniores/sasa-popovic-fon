<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Exam application confirmation</title>
<style>
.flex-container{
		display: flex;
		justify-content: center;
		align-items: center;
		height: 100vh;
		flex-wrap: nowrap;
		flex-direction: column;
	}
</style>
</head>
<body>
	<h1 class="flex-container">EXAM APPLICATION IS SAVED!</h1>
</body>
</html>