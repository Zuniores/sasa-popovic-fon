<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<style>
/* Style the body */
body {
	font-family: Arial;
	margin: 0;
}

/* Header/Logo Title */
/* .header { */
/* 	padding: 60px; */
/* 	text-align: center; */
/* 	background: #1abc9c; */
/* 	color: white; */
/* 	font-size: 30px; */
/* } */
.flex-container{
		display: flex;
		justify-content: center;
		align-items: center;
		height: 100vh;
		flex-wrap: nowrap;
		flex-direction: column;
	}
/* Page Content */
.content {
	padding: 20px;
}
</style>
</head>
<body>


	<div class="header flex-container">
		<h1>SPRING WEB APPLICATION</h1>
		<p>by Sasha Popovic</p>
	</div>