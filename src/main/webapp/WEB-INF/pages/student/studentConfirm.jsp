<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Add student</title>
<!-- Bootstrap core CSS -->
<!-- <link href="resources/bootstrap/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
<!-- <link href="resources/jquery/jquery-ui-1.12.1/jquery-ui-1.12.1.custom/jquery-ui.min.css" rel="stylesheet"> -->
<style>
<%@include file="../resources/bootstrap/bootstrap/css/bootstrap.min.css" %>
<%@include file="../resources/jquery/jquery-ui-1.12.1/jquery-ui-1.12.1.custom/jquery-ui.min.css" %>
</style>
</head>

	<c:set value="${pageContext.request.contextPath}" var="contextPath"></c:set>

<body data-gr-c-s-loaded="true">

	<div class="container fill">
		<div class="row">
			<div class="col-md-7">
				<br>
				<div class="card mb-4">
					<div class="card-body">
						<h2 class="card-title">Students</h2>
						<c:if test="${not empty errorMessage }">
							<div>${errorMessage}</div>
						</c:if>
						<form:form class="form-horizontal" action="${contextPath}/student/save" method="post"
							modelAttribute="studentDto">
							<fieldset>
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="indexNumber">Index number: </label>
									<div class="col-sm-8">
										<form:input type="text" class="form-control" path="indexNumber" id="indexNumber" required="required" disabled="true"/>
										<div class="invalid-feedback"></div>
										<form:errors path="indexNumber" cssClass="error" />
									</div>
								</div>
								
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="firstname">First Name: </label>
									<div class="col-sm-8">
										<form:input type="text" class="form-control" path="firstname" id="firstname" required="required" disabled="true"/>
										<div class="invalid-feedback"></div>
										<form:errors path="firstname" cssClass="error" />
									</div>
								</div>
								
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="lastname">Last Name: </label>
									<div class="col-sm-8">
										<form:input type="text" class="form-control" path="lastname" id="lastname" required="required" disabled="true"/>
										<div class="invalid-feedback"></div>
										<form:errors path="lastname" cssClass="error" />
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="email">Email: </label>
									<div class="col-sm-8">
										<form:input type="text" class="form-control" path="email" id="email" required="" disabled="true"/>
										<div class="invalid-feedback"></div>
										<form:errors path="email" cssClass="error" />
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="address">Address: </label>
									<div class="col-sm-8">
										<form:input type="text" class="form-control" path="address" id="address" required="" disabled="true"/>
										<div class="invalid-feedback"></div>
										<form:errors path="address" cssClass="error" />
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="cityDto">City</label>
									<div class="col-sm-8">
										<form:select id="cityDto" path="cityDto" class="form-control">
											<form:options items="${cityDtos}" itemValue="id" itemLabel="name" disabled="true"/>
										</form:select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="phone">Phone number: </label>
									<div class="col-sm-8">
										<form:input type="text" class="form-control" path="phone" id="phone" required="" disabled="true"/>
										<div class="invalid-feedback"></div>
										<form:errors path="phone" cssClass="error" />
									</div>
								</div>
<!-- 								<div class="form-group row"> -->
<!-- 									<label class="col-sm-3 col-form-label" for="currentYearOfStudy">Current year of study:</label> -->
<!-- 									<div class="col-sm-8"> -->
<%-- 										<form:select id="currentYearOfStudy" path="currentYearOfStudy" class="form-control"> --%>
<%-- 											<form:option value="" selected="true" disabled="true" disabled="true">Choose here</form:option> --%>
<%-- 											<form:option value="1">First</form:option> --%>
<%-- 											<form:option value="2">Second</form:option> --%>
<%-- 											<form:option value="3">Third</form:option> --%>
<%-- 											<form:option value="4">Forth</form:option> --%>
<%-- 											<form:options itemValue="currentYearOfStudy" itemLabel="currentYearOfStudy"/> --%>
<%-- 										</form:select> --%>
<!-- 									</div> -->
<!-- 								</div> -->
								
<!-- 								<div class="form-group row"> -->
<!-- 									<label class="col-sm-3 col-form-label" for="country">Country</label> -->
<!-- 									<div class="col-sm-8"> -->
<!-- 										<select id="country" name="country" class="form-control"> -->
<!-- 											<option value="" selected disabled hidden>Choose here</option> -->
<!-- 											<option value="Serbia">Serbia</option> -->
<!-- 											<option value="USA">USA</option> -->
<!-- 											<option value="UK">UK</option> -->
<!-- 											<option value="Russia">Russia</option> -->
<!-- 										</select> -->
<!-- 									</div> -->
<!-- 								</div> -->

<!-- 								<div class="form-group row"> -->
<!-- 									<label class="col-sm-3 col-form-label" for="city">City: </label> -->
<!-- 									<div class="col-sm-8"> -->
<!-- 										<input type="text" class="form-control" id="city" required=""> -->
<!-- 										<div class="invalid-feedback">City is invalid!</div> -->
<!-- 									</div> -->
<!-- 								</div> -->

<!-- 								<div class="form-group row"> -->
<!-- 									<label class="col-sm-3 col-form-label" for="phonenumber">Phone number: </label> -->
<!-- 									<div class="col-sm-8"> -->
<!-- 										<input type="text" class="form-control" id="phonenumber" required=""> -->
<!-- 										<div class="invalid-feedback">Phone number is invalid!</div> -->
<!-- 									</div> -->
<!-- 								</div> -->
								<div class="form-group" style="display: inline-flex">
									<div class="col-md-4">
										<button id="save" name="submitForm" class="btn btn-primary" value="save" type="submit">Save</button>
									</div>
									<div class="col-md-4">
										<button id="change" name="submitForm" class="btn btn-primary" value="change" type="submit">Change</button>
									</div>
									<div class="col-md-4">
										<button id="cancel" name="submitForm" class="btn btn-primary" value="cancel" type="submit">Cancel</button>
									</div>
								</div>
							</fieldset>
						</form:form>

					</div>
					
				</div>
			</div>

			<div class="col-md-5">
				<!-- Search Widget -->
				<div class="card my-4">
					<h5 class="card-header">Students</h5>
					<div class="card-body">
						<div class="row">
							<table class="table" id="employees">
								<thead>
									<tr>
										<th scope="col">#</th>
										<th scope="col">First name</th>
										<th scope="col">Last name</th>
										<th scope="col">Index number</th>
										<th scope="col">Email</th>
									</tr>
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<footer class="py-5 bg-dark">
		<div class="container">
			<p class="m-0 text-center text-white">Copyright © Your Website 2019</p>
		</div>
	</footer>

<!-- 	<script src="resources/jquery/jquery-3.4.1.min.js"></script> -->
<!-- 	<script src="resources/bootstrap/bootstrap/js/bootstrap.bundle.min.js"></script> -->
<!-- 	<script src="resources/jquery/jquery-ui-1.12.1/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script> -->
	
	<script>
		
		
	</script>
</body>
</html>