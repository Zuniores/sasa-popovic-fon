<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Student list</title>
<!-- Bootstrap core CSS -->
<!-- <link href="resources/bootstrap/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
<!-- <link href="resources/jquery/jquery-ui-1.12.1/jquery-ui-1.12.1.custom/jquery-ui.min.css" rel="stylesheet"> -->
<style>
<%@include file="../resources/bootstrap/bootstrap/css/bootstrap.min.css" %>
<%@include file="../resources/jquery/jquery-ui-1.12.1/jquery-ui-1.12.1.custom/jquery-ui.min.css" %>
.col-md-8 {
	-webkit-flex: 0 0 100%;
	-moz-flex: 0 0 100%;
    -ms-flex: 0 0 100%;
    flex: 0 0 100%;
    max-width: 100%;
}
</style>
</head>
	
	<c:set value="${pageContext.request.contextPath}" var="contextPath"></c:set>
	<c:url var="editStudentURL" value="/student/edit"/>

<body data-gr-c-s-loaded="true">
	<div class="container fill">
		<div class="row" style="box-sizing: unset">
			<div class="col-md-8" style ="max-width: 100%">
				<br>
				<div class="card mb-4">
					<div class="card-body">
						<h2 class="card-title">Students</h2>
						<table class="table" id="employees">
							<thead>
								<tr>
									<th scope="col">Index number</th>
									<th scope="col">First name</th>
									<th scope="col">Last name</th>
									<th scope="col">Email</th>
									<th scope="col">Address</th>
									<th scope="col">City</th>
									<th scope="col">Phone number</th>
									<th scope="col">Current year of study</th>
									<th scope="col">Modify student</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${studentDtos}" var="studentDto">
									<tr>
										<td><p>${studentDto.getIndexNumber()}</p><br/></td>
										<td><p>${studentDto.getFirstname()}</p><br/></td>
										<td><p>${studentDto.getLastname()}</p><br/></td>
										<td><p>${studentDto.getEmail()}</p><br/></td>
										<td><p>${studentDto.getAddress()}</p><br/></td>
										<td><p>${studentDto.getCityDto().getName()}</p><br/></td>
										<td><p>${studentDto.getPhone()}</p><br/></td>
										<td><p>${studentDto.getCurrentYearOfStudy()}</p><br/></td>
										<td>
											<form:form method="post" action="/mywebproject/student/edit" modelAttribute="studentDto">
												<fieldset>
													<form:input type = "hidden" id="id" path = "id" value = "${studentDto.getId()}" />
														<button id="btnSubmitForm" name="submitForm" class="btn btn-primary" type="submit">Edit</button>
<%-- 												<form:input type = "submit" value="edit"/> --%>
												</fieldset>
											</form:form>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<nav>
							<ul class="pagination justify-content-center">
								<li class="page-item active"><a class="page-link">1</a></li>
								<li class="page-item"><a class="page-link">2</a></li>
								<li class="page-item"><a class="page-link">3</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>	
	</div>
	<!-- Sidebar Widgets Column -->
<!-- 	<script src="resources/jquery/jquery-3.4.1.min.js"></script> -->
<!-- 	<script src="resources/bootstrap/bootstrap/js/bootstrap.bundle.min.js"></script> -->
<!-- 	<script src="resources/jquery/jquery-ui-1.12.1/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script> -->
	<script>
	
		
	</script>
</body>
</html>