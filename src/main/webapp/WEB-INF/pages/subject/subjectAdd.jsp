<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<style>
<%@include file="../resources/bootstrap/bootstrap/css/bootstrap.min.css" %>
<%@include file="../resources/jquery/jquery-ui-1.12.1/jquery-ui-1.12.1.custom/jquery-ui.min.css" %>
</style>
</head>

	<c:set value="${pageContext.request.contextPath}" var="contextPath"></c:set>

<body data-gr-c-s-loaded="true">

	<div class="container fill">
		<div class="row">
			<div class="col-md-7">
				<br>
				<div class="card mb-4">
					<div class="card-body">
						<h2 class="card-title">Add subject</h2>
						
						<form:form class="form-horizontal" action="${contextPath}/subject/save" method="post"
							modelAttribute="subjectDto">
							<fieldset>
<!-- 								<div class="form-group row"> -->
<!-- 									<label class="col-sm-3 col-form-label" for="id">Id: </label> -->
<!-- 									<div class="col-sm-8"> -->
<%-- 										<form:input type="text" class="form-control" path="id" id="id" required="" /> --%>
<!-- 										<div class="invalid-feedback"></div> -->
<%-- 										<form:errors path="id" cssClass="error" /> --%>
<!-- 									</div> -->
<!-- 								</div> -->
								
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="name">Name: </label>
									<div class="col-sm-8">
										<form:input type="text" class="form-control" path="name" id="name" required="" />
										<div class="invalid-feedback"></div>
										<form:errors path="name" cssClass="error" />
									</div>
								</div>
								
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="description">Description: </label>
									<div class="col-sm-8">
										<form:input type="text" class="form-control" path="description" id="description" required="" />
										<div class="invalid-feedback"></div>
										<form:errors path="description" cssClass="error" />
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="yearOfStudy">Current year of study:</label>
									<div class="col-sm-8">
										<form:select id="yearOfStudy" path="yearOfStudy" class="form-control">
											<form:option value="1" selected="true">First</form:option>
											<form:option value="2">Second</form:option>
											<form:option value="3">Third</form:option>
											<form:option value="4">Forth</form:option>
											<form:options itemValue="yearOfStudy" itemLabel="yearOfStudy"/>
										</form:select>
									</div>
								</div>
								
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="semester">Semester: </label>
									<div class="col-sm-8">
										<form:select id="semester" path="semester" class="form-control">
											<form:options items="${semesterValues}" />
										</form:select>
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-4">
										<button id="btnSubmitForm" name="submitForm" class="btn btn-primary" type="submit">Save</button>
									</div>
								</div>
							</fieldset>
						</form:form>

					</div>
					
				</div>
			</div>

			<div class="col-md-5">
				<!-- Search Widget -->
				<div class="card my-4">
					<h5 class="card-header">Subjects</h5>
					<div class="card-body">
						<div class="row">
							<table class="table" id="employees">
								<thead>
									<tr>
										<th scope="col">#</th>
										<th scope="col">Name</th>
										<th scope="col">Year of study</th>
										<th scope="col">Semester</th>
									</tr>
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<footer class="py-5 bg-dark">
		<div class="container">
			<p class="m-0 text-center text-white">Copyright © Your Website 2020</p>
		</div>
	</footer>

	<script src="resources/jquery/jquery-3.4.1.min.js"></script>
	<script src="resources/bootstrap/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="resources/jquery/jquery-ui-1.12.1/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
	
	<script>
		
		
	</script>
</body>
</html>