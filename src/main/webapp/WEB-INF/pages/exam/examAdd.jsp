<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Add exam</title>
<!-- Bootstrap core CSS -->
<!-- <link href="resources/bootstrap/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
<!-- <link href="resources/jquery/jquery-ui-1.12.1/jquery-ui-1.12.1.custom/jquery-ui.min.css" rel="stylesheet"> -->
<style>
<%@include file="../resources/bootstrap/bootstrap/css/bootstrap.min.css" %>
<%@include file="../resources/jquery/jquery-ui-1.12.1/jquery-ui-1.12.1.custom/jquery-ui.min.css" %>
</style>
</head>

	<c:set value="${pageContext.request.contextPath}" var="contextPath"></c:set>

<body data-gr-c-s-loaded="true">

	<div class="container fill">
		<div class="row">
			<div class="col-md-7">
				<br>
				<div class="card mb-4">
					<div class="card-body">
						<h2 class="card-title">Add exam</h2>
						<c:if test="${not empty errorMessage }">
							<div>${errorMessage}</div>
						</c:if>
						<c:if test="${not empty errorExam }">
							<div>${errorExam}</div>
						</c:if>
						<form:form class="form-horizontal" action="${contextPath}/exam/save" method="post"
							modelAttribute="examDto" autocomplete="on">
							<fieldset>
<!-- 								<div class="form-group row"> -->
<!-- 									<label class="col-sm-3 col-form-label" for="id">Id: </label> -->
<!-- 									<div class="col-sm-8"> -->
<%-- 										<form:input type="text" class="form-control" path="id" id="id" required="" /> --%>
<!-- 										<div class="invalid-feedback"></div> -->
<%-- 										<form:errors path="id" cssClass="error" /> --%>
<!-- 									</div> -->
<!-- 								</div> -->

								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="professorDto">Professor: </label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="professorDto"></input>
										<form:input type="hidden" class="form-control" path="professorDto" id="id"/>
										<div class="invalid-feedback"></div>
										<form:errors path="professorDto" cssClass="error" />
									</div>
								</div>
								
<!-- 								<div class="form-group row"> -->
<!-- 									<label class="col-sm-3 col-form-label" for="professorDto">Professor: </label> -->
<!-- 									<div class="col-sm-8"> -->
<%-- 										<form:select id="professorDto" path="professorDto" class="form-control"> --%>
<%-- 											<form:options items="${professorDtos}" itemValue="id"/> --%>
<%-- 										</form:select> --%>
<!-- 									</div> -->
<!-- 								</div> -->
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="subjectDto">Subject: </label>
									<div class="col-sm-8">
										<form:select id="subjectDto" path="subjectDto" class="form-control">
											<form:options items="${subjectDtos}" itemValue="id"/>
										</form:select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="date">Date: </label>
									<div class="col-sm-8">
										<form:input type="date" class="form-control" path="date" id="date" required="" />
										<div class="invalid-feedback"></div>
										<form:errors path="date" cssClass="error" />
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-4">
										<button id="btnSubmitForm" name="submitForm" class="btn btn-primary" type="submit">Save</button>
									</div>
								</div>
							</fieldset>
						</form:form>

					</div>
					
				</div>
			</div>

			<div class="col-md-5">
				<!-- Search Widget -->
				<div class="card my-4">
					<h5 class="card-header">Exams</h5>
					<div class="card-body">
						<div class="row">
							<table class="table" id="employees">
								<thead>
									<tr>
										<th scope="col">#</th>
										<th scope="col">Professor</th>
										<th scope="col">Subject</th>
										<th scope="col">Date</th>
									</tr>
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<footer class="py-5 bg-dark">
		<div class="container">
			<p class="m-0 text-center text-white">Copyright © Your Website 2019</p>
		</div>
	</footer>

<!-- 	<script src="resources/jquery/jquery-3.4.1.min.js"></script> -->
<!-- 	<script src="resources/bootstrap/bootstrap/js/bootstrap.bundle.min.js"></script> -->
<!-- 	<script src="resources/jquery/jquery-ui-1.12.1/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script> -->
	
	<script>
		function autocomplete(inp, hid, arr) {
		  /*the autocomplete function takes two arguments,
		  the text field element and an array of possible autocompleted values:*/
		  var currentFocus;
		  /*execute a function when someone writes in the text field:*/
		  inp.addEventListener("input", function(e) {
		      var a, b, i, val = this.value;
		      /*close any already open lists of autocompleted values*/
		      closeAllLists();
		      if (!val) { return false;}
		      currentFocus = -1;
		      /*create a DIV element that will contain the items (values):*/
		      a = document.createElement("DIV");
		      a.setAttribute("id", this.id + "autocomplete-list");
		      a.setAttribute("class", "autocomplete-items");
		      /*append the DIV element as a child of the autocomplete container:*/
		      this.parentNode.appendChild(a);
		      /*for each item in the array...*/
		      
		      for(var key in arr) {
// 		      for (i = 0; i < arr.length; i++) {
		        /*check if the item starts with the same letters as the text field value:*/
		        if (key.substr(0, val.length).toUpperCase() == val.toUpperCase()) {
		          /*create a DIV element for each matching element:*/
		          b = document.createElement("DIV");
		          /*make the matching letters bold:*/
		          b.innerHTML = "<strong>" + key.substr(0, val.length) + "</strong>";
		          b.innerHTML += key.substr(val.length);
		          /*insert a input field that will hold the current array item's value:*/
		          b.innerHTML += "<input type='hidden' value='" + key + "'>";
		          /*execute a function when someone clicks on the item value (DIV element):*/
		          b.addEventListener("click", function(e) {
		              /*insert the value for the autocomplete text field:*/
		              inp.value = this.getElementsByTagName("input")[0].value;
		              hid.value = arr[key];
		              /*close the list of autocompleted values,
		              (or any other open lists of autocompleted values:*/
		              closeAllLists();
		          });
		          a.appendChild(b);
		        }
		      }
		  });
		  /*execute a function presses a key on the keyboard:*/
		  inp.addEventListener("keydown", function(e) {
		      var x = document.getElementById(this.id + "autocomplete-list");
		      if (x) x = x.getElementsByTagName("div");
		      if (e.keyCode == 40) {
		        /*If the arrow DOWN key is pressed,
		        increase the currentFocus variable:*/
		        currentFocus++;
		        /*and and make the current item more visible:*/
		        addActive(x);
		      } else if (e.keyCode == 38) { //up
		        /*If the arrow UP key is pressed,
		        decrease the currentFocus variable:*/
		        currentFocus--;
		        /*and and make the current item more visible:*/
		        addActive(x);
		      } else if (e.keyCode == 13) {
		        /*If the ENTER key is pressed, prevent the form from being submitted,*/
		        e.preventDefault();
		        if (currentFocus > -1) {
		          /*and simulate a click on the "active" item:*/
		          if (x) x[currentFocus].click();
		        }
		      }
		  });
		  function addActive(x) {
		    /*a function to classify an item as "active":*/
		    if (!x) return false;
		    /*start by removing the "active" class on all items:*/
		    removeActive(x);
		    if (currentFocus >= x.length) currentFocus = 0;
		    if (currentFocus < 0) currentFocus = (x.length - 1);
		    /*add class "autocomplete-active":*/
		    x[currentFocus].classList.add("autocomplete-active");
		  }
		  function removeActive(x) {
		    /*a function to remove the "active" class from all autocomplete items:*/
		    for (var i = 0; i < x.length; i++) {
		      x[i].classList.remove("autocomplete-active");
		    }
		  }
		  function closeAllLists(elmnt) {
		    /*close all autocomplete lists in the document,
		    except the one passed as an argument:*/
		    var x = document.getElementsByClassName("autocomplete-items");
		    for (var i = 0; i < x.length; i++) {
		      if (elmnt != x[i] && elmnt != inp) {
		        x[i].parentNode.removeChild(x[i]);
		      }
		    }
		  }
		  /*execute a function when someone clicks in the document:*/
		  document.addEventListener("click", function (e) {
		      closeAllLists(e.target);
		  });
		}
		
		
		var professorDtos = {<c:forEach items="${professorDtos}" var="professorDto">
								"<c:out value="${professorDto}" />":  "<c:out value="${professorDto.getId()}" />",
							</c:forEach>}
		autocomplete(document.getElementById("professorDto"), document.getElementById("id"), professorDtos);
</script>
	
	
</body>
</html>