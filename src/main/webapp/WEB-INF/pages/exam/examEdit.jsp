<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<!-- Bootstrap core CSS -->
<!-- <link href="resources/bootstrap/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
<!-- <link href="resources/jquery/jquery-ui-1.12.1/jquery-ui-1.12.1.custom/jquery-ui.min.css" rel="stylesheet"> -->
<style>
<%@include file="../resources/bootstrap/bootstrap/css/bootstrap.min.css" %>
<%@include file="../resources/jquery/jquery-ui-1.12.1/jquery-ui-1.12.1.custom/jquery-ui.min.css" %>
</style>
</head>

	<c:set value="${pageContext.request.contextPath}" var="contextPath"></c:set>

<body data-gr-c-s-loaded="true">

	<div class="container fill">
		<div class="row">
			<div class="col-md-7">
				<br>
				<div class="card mb-4">
					<div class="card-body">
						<h2 class="card-title">Edit exam</h2>
						<c:if test="${not empty errorMessage }">
							<div>${errorMessage}</div>
						</c:if>
						<form:form class="form-horizontal" action="${contextPath}/exam/update" method="post" modelAttribute="examDto">
							<fieldset>
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="id" hidden="true">Id: </label>
									<div class="col-sm-8">
										<form:input type="text" class="form-control" path="id" id="id" 
										value = "${examDto.getId()}" hidden="true"/>
										<div class="invalid-feedback"></div>
<%-- 										<form:errors path="id" cssClass="error" /> --%>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="professorDto">Professor: </label>
									<div class="col-sm-8">
										<form:select id="professorDto" path="professorDto" name ="professorDto" class="form-control">
											<form:options items="${professorDtos}" itemValue="id" />
										</form:select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="subjectDto">Subject: </label>
									<div class="col-sm-8">
										<form:select id="subjectDto" path="subjectDto" name="subjectDto" class="form-control">
											<form:options items="${subjectDtos}" itemValue="id" />
										</form:select>
									</div>
								</div>
								
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="date">Date: </label>
									<div class="col-sm-8">
										<form:input type="date" class="form-control" path="date" id="date" required="" />
										<div class="invalid-feedback"></div>
										<form:errors path="date" cssClass="error" />
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-md-4">
										<button id="btnSubmitForm" name="submitForm" class="btn btn-primary" type="submit">Update</button>
									</div>
								</div>
							</fieldset>
						</form:form>

					</div>
					
				</div>
			</div>
			<div class="col-md-5">
				<!-- Search Widget -->
				<div class="card my-4">
					<h5 class="card-header">Exams</h5>
					<div class="card-body">
						<div class="row">
							<table class="table" id="employees">
								<thead>
									<tr>
										<th scope="col">#</th>
										<th scope="col">Professor</th>
										<th scope="col">Subject</th>
										<th scope="col">Date</th>
									</tr>
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<!-- 	<script src="resources/jquery/jquery-3.4.1.min.js"></script> -->
<!-- 	<script src="resources/bootstrap/bootstrap/js/bootstrap.bundle.min.js"></script> -->
<!-- 	<script src="resources/jquery/jquery-ui-1.12.1/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script> -->
	
	<script>
		
		
	</script>
</body>
</html>