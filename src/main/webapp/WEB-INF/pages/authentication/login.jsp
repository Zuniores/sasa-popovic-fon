<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Index</title>
<style type="text/css">
body {
	margin: 0;
}

.flex-container {
/* 	display: flex; */
	justify-content: center;
	align-items: center;
	height: 100vh;
}

@media all and (max-width: 500px) {
	.flex-container {
		flex-direction: column;
	}
}
</style>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" ></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

<!-- <link rel='stylesheet' -->
<!-- 	href='webjars/bootstrap/4.4.1/css/bootstrap.min.css'> -->
<!-- </head> -->
<body>
	<c:set value="${pageContext.request.contextPath}" var="contextPath"></c:set>

	<div class="flex-container">
		<div class="row">
			<div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
				<div class="card card-signin my-5">
					<div class="card-body">
						<h5 class="card-title text-center">Sign In</h5>
						<c:if test="${not empty errorMessage }">
							<div>${errorMessage}</div>
						</c:if>
						<form:form class="form-signin"
							action="${contextPath}/authentication/login" method="post"
							modelAttribute="employeeDto">
							<div class="form-label-group">

								<form:input type="text" path="username" id="username"
									name="username" class="form-control"
									placeholder="Username" required="required"
									autofocus="autofocus" />
								<label for="username">Username</label>
								<form:errors path="username" cssClass="error" />
							</div>

							<div class="form-label-group">
								<form:input type="password" path="password" id="password"
									name="password" class="form-control" placeholder="Password"
									required="required" />
								<label for="password">Password</label>
								<form:errors path="password" cssClass="error" />
							</div>


							<button class="btn btn-lg btn-primary btn-block text-uppercase"
								type="submit">Sign in</button>
						</form:form>

					</div>
				</div>
			</div>
		</div>
	</div>
</body>

<script type="text/javascript" src="webjars/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript"
	src="webjars/bootstrap/4.4.1/js/bootstrap.min.js"></script>


</html>