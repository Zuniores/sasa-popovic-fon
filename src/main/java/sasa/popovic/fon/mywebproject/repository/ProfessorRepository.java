package sasa.popovic.fon.mywebproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sasa.popovic.fon.mywebproject.domain.Professor;

@Repository
public interface ProfessorRepository extends JpaRepository<Professor, Long>{
	List<Professor> findByEmail(String email);
}
