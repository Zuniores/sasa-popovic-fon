package sasa.popovic.fon.mywebproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sasa.popovic.fon.mywebproject.domain.ExamStudent;

@Repository
public interface ExamStudentRepository extends JpaRepository<ExamStudent, Long>{
	
//	@Query("SELECT e FROM ExamStudent e WHERE enrollDate BETWEEN DATEADD(day, -7, GETDATE()) AND GETDATE()")
//	ExamStudent checkDate(String enrollDate);
}
