package sasa.popovic.fon.mywebproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sasa.popovic.fon.mywebproject.domain.Title;

@Repository
public interface TitleRepository extends JpaRepository<Title, Long>{
	
}
