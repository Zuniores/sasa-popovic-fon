package sasa.popovic.fon.mywebproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sasa.popovic.fon.mywebproject.domain.Subject;

@Repository
public interface SubjectRepository extends JpaRepository<Subject, Long>{
	
}
