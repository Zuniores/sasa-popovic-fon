package sasa.popovic.fon.mywebproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sasa.popovic.fon.mywebproject.domain.City;

@Repository
public interface CityRepository extends JpaRepository<City, Long>{
	List<City> findByNumber(Long number);
}
