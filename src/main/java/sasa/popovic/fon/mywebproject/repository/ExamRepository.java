package sasa.popovic.fon.mywebproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sasa.popovic.fon.mywebproject.domain.Exam;

@Repository
public interface ExamRepository extends JpaRepository<Exam, Long>{

}
