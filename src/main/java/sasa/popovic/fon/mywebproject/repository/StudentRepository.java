package sasa.popovic.fon.mywebproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sasa.popovic.fon.mywebproject.domain.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long>{
	
//	@Query("SELECT s FROM Student s WHERE s.indexNumber=?1")
	List<Student> findByindexNumber(String indexNumber);
	List<Student> findByEmail(String email);
}
