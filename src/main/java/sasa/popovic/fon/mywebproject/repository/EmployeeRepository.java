package sasa.popovic.fon.mywebproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sasa.popovic.fon.mywebproject.domain.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, String>{
	List<Employee> findByUsername(String username);
}
