package sasa.popovic.fon.mywebproject.formatter;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import sasa.popovic.fon.mywebproject.dto.CityDto;
import sasa.popovic.fon.mywebproject.service.CityService;


public class CityDtoFormatter implements Formatter<CityDto>{
	private final CityService cityService;
	
	@Autowired
	public CityDtoFormatter(CityService cityService) {
		this.cityService = cityService;
	}
	
	@Override
	public String print(CityDto cityDto, Locale locale) {
		System.out.println("=======CityDtoFormatter: print============================================");
		System.out.println(cityDto);
		System.out.println("==========================================================================");
		return cityDto.toString();
	}

	@Override
	public CityDto parse(String city, Locale locale) {
		System.out.println("=======CityDtoFormatter: parse============================================");
		System.out.println(city);
		System.out.println("==========================================================================");
		
		Long number=Long.parseLong(city);
		System.out.println(number);
		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		CityDto cityDto = new CityDto();
		try {
			cityDto = cityService.findById(number);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("==========================================================================");
		return cityDto;
	}

}
