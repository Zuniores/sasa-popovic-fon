package sasa.popovic.fon.mywebproject.formatter;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import sasa.popovic.fon.mywebproject.dto.CityDto;
import sasa.popovic.fon.mywebproject.dto.SubjectDto;
import sasa.popovic.fon.mywebproject.service.CityService;
import sasa.popovic.fon.mywebproject.service.SubjectService;


public class SubjectDtoFormatter implements Formatter<SubjectDto>{
	private final SubjectService subjectService;
	
	@Autowired
	public SubjectDtoFormatter(SubjectService subjectService) {
		this.subjectService = subjectService;
	}
	
	@Override
	public String print(SubjectDto subjectDto, Locale locale) {
		System.out.println("=======SubjectDtoFormatter: print============================================");
		System.out.println(subjectDto);
		System.out.println("==========================================================================");
		return subjectDto.toString();
	}

	@Override
	public SubjectDto parse(String id, Locale locale) {
		System.out.println("=======SubjectDtoFormatter: parse============================================");
		System.out.println(id);
		System.out.println("==========================================================================");
		
		Long subjectId=Long.parseLong(id);
		System.out.println(subjectId);
		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		SubjectDto subjectDto = new SubjectDto();
		try {
			subjectDto = subjectService.findById(subjectId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("==========================================================================");
		return subjectDto;
	}

}
