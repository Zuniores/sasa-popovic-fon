package sasa.popovic.fon.mywebproject.formatter;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import sasa.popovic.fon.mywebproject.dto.ExamStudentDto;
import sasa.popovic.fon.mywebproject.service.ExamStudentService;

public class ExamStudentDtoFormatter implements Formatter<ExamStudentDto> {
	private final ExamStudentService examStudentService;

	@Autowired
	public ExamStudentDtoFormatter(ExamStudentService examStudentService) {
		this.examStudentService = examStudentService;
	}

	@Override
	public String print(ExamStudentDto examStudentDto, Locale locale) {
		System.out.println("=======ExamStudentDtoFormatter: print============================================");
		System.out.println(examStudentDto);
		System.out.println("==========================================================================");
		return examStudentDto.toString();
	}

	@Override
	public ExamStudentDto parse(String id, Locale locale) {
		System.out.println("=======ExamStudentDtoFormatter: parse============================================");
		System.out.println(id);
		System.out.println("==========================================================================");

		Long examId = Long.parseLong(id);
		System.out.println(id);
		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		ExamStudentDto examStudentDto = new ExamStudentDto();
		try {
			examStudentDto = examStudentService.findById(examId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("==========================================================================");
		return examStudentDto;
	}

}
