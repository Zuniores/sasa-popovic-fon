package sasa.popovic.fon.mywebproject.formatter;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import sasa.popovic.fon.mywebproject.dto.CityDto;
import sasa.popovic.fon.mywebproject.dto.TitleDto;
import sasa.popovic.fon.mywebproject.service.CityService;
import sasa.popovic.fon.mywebproject.service.TitleService;


public class TitleDtoFormatter implements Formatter<TitleDto>{
	private final TitleService titleService;
	
	@Autowired
	public TitleDtoFormatter(TitleService titleService) {
		this.titleService = titleService;
	}
	
	@Override
	public String print(TitleDto titleDto, Locale locale) {
		System.out.println("=======TitleDtoFormatter: print============================================");
		System.out.println(titleDto);
		System.out.println("==========================================================================");
		return titleDto.toString();
	}

	@Override
	public TitleDto parse(String title, Locale locale) {
		System.out.println("=======TitleDtoFormatter: parse============================================");
		System.out.println(title);
		System.out.println("==========================================================================");
		
		Long id=Long.parseLong(title);
		System.out.println(id);
		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		TitleDto titleDto = new TitleDto();
		try {
			titleDto = titleService.findById(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("==========================================================================");
		return titleDto;
	}

}
