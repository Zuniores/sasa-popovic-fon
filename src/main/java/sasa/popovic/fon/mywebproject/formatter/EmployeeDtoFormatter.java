package sasa.popovic.fon.mywebproject.formatter;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import sasa.popovic.fon.mywebproject.dto.EmployeeDto;
import sasa.popovic.fon.mywebproject.service.EmployeeService;

public class EmployeeDtoFormatter implements Formatter<EmployeeDto>{
	private final EmployeeService employeeService;
	
	@Autowired
	public EmployeeDtoFormatter(EmployeeService employeeService) {
		this.employeeService = employeeService;
	}
	
	@Override
	public String print(EmployeeDto employeeDto, Locale locale) {
		System.out.println("=======EmployeeDtoFormatter: print============================================");
		System.out.println(employeeDto);
		System.out.println("==========================================================================");
		return employeeDto.toString();
	}

	@Override
	public EmployeeDto parse(String employee, Locale locale) {
		System.out.println("=======EmployeeDtoFormatter: parse============================================");
		System.out.println(employee);
		System.out.println("==========================================================================");
		
		String username = employee;
		System.out.println(username);
		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		EmployeeDto employeeDto = employeeService.findByUsername(username);
		System.out.println("==========================================================================");
		return employeeDto;
	}

}
