package sasa.popovic.fon.mywebproject.formatter;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import sasa.popovic.fon.mywebproject.dto.EmployeeDto;
import sasa.popovic.fon.mywebproject.dto.ProfessorDto;
import sasa.popovic.fon.mywebproject.dto.StudentDto;
import sasa.popovic.fon.mywebproject.service.EmployeeService;
import sasa.popovic.fon.mywebproject.service.ProfessorService;
import sasa.popovic.fon.mywebproject.service.StudentService;

public class ProfessorDtoFormatter implements Formatter<ProfessorDto>{
	private final ProfessorService professorService;
	
	@Autowired
	public ProfessorDtoFormatter(ProfessorService professorService) {
		this.professorService = professorService;
	}
	
	@Override
	public String print(ProfessorDto professorDto, Locale locale) {
		System.out.println("=======ProfessorDtoFormatter: print============================================");
		System.out.println(professorDto);
		System.out.println("==========================================================================");
		return professorDto.toString();
	}

	@Override
	public ProfessorDto parse(String professorId, Locale locale) {
		System.out.println("=======ProfessorDtoFormatter: parse============================================");
		System.out.println(professorId);
		System.out.println("==========================================================================");
		Long id = Long.parseLong(professorId);
		ProfessorDto professorDto = new ProfessorDto();
		try {
			professorDto = professorService.findById(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return professorDto;
	}

}
