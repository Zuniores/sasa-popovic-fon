package sasa.popovic.fon.mywebproject.formatter;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import sasa.popovic.fon.mywebproject.dto.EmployeeDto;
import sasa.popovic.fon.mywebproject.dto.StudentDto;
import sasa.popovic.fon.mywebproject.service.EmployeeService;
import sasa.popovic.fon.mywebproject.service.StudentService;

public class StudentDtoFormatter implements Formatter<StudentDto>{
	private final StudentService studentService;
	
	@Autowired
	public StudentDtoFormatter(StudentService studentService) {
		this.studentService = studentService;
	}
	
	@Override
	public String print(StudentDto studentDto, Locale locale) {
		System.out.println("=======StudentDtoFormatter: print============================================");
		System.out.println(studentDto);
		System.out.println("==========================================================================");
		return studentDto.toString();
	}

	@Override
	public StudentDto parse(String studentId, Locale locale) {
		System.out.println("=======StudentDtoFormatter: parse============================================");
		System.out.println(studentId);
		System.out.println("==========================================================================");
		Long id = Long.parseLong(studentId);
		StudentDto studentDto = new StudentDto();
		try {
			studentDto = studentService.findById(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("==========================================================================");
		return studentDto;
	}

}
