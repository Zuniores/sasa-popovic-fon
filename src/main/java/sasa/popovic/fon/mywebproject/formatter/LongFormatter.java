package sasa.popovic.fon.mywebproject.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.format.Formatter;

import sasa.popovic.fon.mywebproject.service.CityService;

public class LongFormatter implements Formatter<Long> {

	public LongFormatter() {
	}

	@Override
	public String print(Long object, Locale locale) {
		return String.valueOf(object);
	}

	@Override
	public Long parse(String text, Locale locale) throws ParseException {
		System.out.println("=======LongFormatter: parse============================================");

		Long number = Long.parseLong(text);
		System.out.println(number);
		return number;
	}

}
