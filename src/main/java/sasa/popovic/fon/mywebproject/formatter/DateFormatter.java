package sasa.popovic.fon.mywebproject.formatter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.springframework.format.Formatter;

public class DateFormatter implements Formatter<Date>{

//	private String pattern;
	
	public DateFormatter() {
//        this.pattern = pattern;
    }
	
	@Override
	public String print(Date date, Locale locale) {
		System.out.println("=======DateFormatter: print============================================");

        if (date == null) {
            return "";
        }
        return getDateFormat(locale).format(date);
    }

	@Override
	public Date parse(String formatted, Locale locale) throws ParseException {
		System.out.println("=======DateFormatter: parse============================================");
		System.out.println("String: " + formatted);
        if (formatted.length() == 0) {
            return null;
        }
       SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd", locale);
       System.out.println("Simple date: " + date);
       return date.parse(formatted);
//        return getDateFormat(locale).parse(formatted);
    }
	
	protected DateFormat getDateFormat(Locale locale) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", locale);
        dateFormat.setLenient(false);
        return dateFormat;
    }

}
