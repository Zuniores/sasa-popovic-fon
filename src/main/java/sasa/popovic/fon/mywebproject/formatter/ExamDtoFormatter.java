package sasa.popovic.fon.mywebproject.formatter;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import sasa.popovic.fon.mywebproject.dto.ExamDto;
import sasa.popovic.fon.mywebproject.service.ExamService;

public class ExamDtoFormatter implements Formatter<ExamDto> {
	private final ExamService examService;

	@Autowired
	public ExamDtoFormatter(ExamService examService) {
		this.examService = examService;
	}

	@Override
	public String print(ExamDto examDto, Locale locale) {
		System.out.println("=======ExamDtoFormatter: print============================================");
		System.out.println(examDto);
		System.out.println("==========================================================================");
		return examDto.toString();
	}

	@Override
	public ExamDto parse(String id, Locale locale) {
		System.out.println("=======ExamDtoFormatter: parse============================================");
		System.out.println(id);
		System.out.println("==========================================================================");

		Long examId = Long.parseLong(id);
		System.out.println(id);
		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		ExamDto examDto = new ExamDto();
		try {
			examDto = examService.findById(examId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("examDto id:" + examDto.getId());
		System.out.println("==========================================================================");
		return examDto;
	}

}
