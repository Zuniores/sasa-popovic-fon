package sasa.popovic.fon.mywebproject.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import sasa.popovic.fon.mywebproject.dto.EmployeeDto;
import sasa.popovic.fon.mywebproject.dto.StudentDto;

public class StudentDtoValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return StudentDto.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		StudentDto studentDto = (StudentDto) target;

		System.out.println("==========  StudentDtoValidator ===============");
		System.out.println(studentDto);
		System.out.println("========================================================");

		if (studentDto != null) {
			if (!(studentDto.getIndexNumber().length() == 10)) {
				errors.rejectValue("indexNumber", "studentDto.indexNumber",
						"Index number have to contain 10 characters!");
			}
			if (studentDto.getFirstname().length() < 3) {
				errors.rejectValue("firstname", "studentDto.firstname",
						"First name has to have a minimum of 3 characters!");
			}
			if (studentDto.getLastname().length() < 3) {
				errors.rejectValue("lastname", "studentDto.lastname",
						"Last name has to have a minimum of 3 characters!");
			}

		}

	}

}
