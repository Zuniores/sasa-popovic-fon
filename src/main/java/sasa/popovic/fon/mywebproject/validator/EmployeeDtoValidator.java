package sasa.popovic.fon.mywebproject.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import sasa.popovic.fon.mywebproject.dto.EmployeeDto;
import sasa.popovic.fon.mywebproject.service.EmployeeService;

public class EmployeeDtoValidator implements Validator {

	@Autowired
	private EmployeeService employeeService;
	
	public EmployeeDtoValidator(EmployeeService employeeService) {
		this.employeeService = employeeService;
	}
	
	@Override
	public boolean supports(Class<?> clazz) {
		return EmployeeDto.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		EmployeeDto employeeDto = (EmployeeDto) target;
		
		System.out.println("==========  EmployeeDtoValidator ===============");
		System.out.println(employeeDto);
		System.out.println("========================================================");
		
		EmployeeDto employee = employeeService.findByUsername(employeeDto.getUsername());
		if(employee == null || !employee.getPassword().equals(employeeDto.getPassword())) {
			errors.rejectValue("username", "employee.username", 
						"There is no employee with provided credentials!");
		}
		
	}

}
