package sasa.popovic.fon.mywebproject.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import sasa.popovic.fon.mywebproject.dto.CityDto;
import sasa.popovic.fon.mywebproject.dto.StudentDto;
import sasa.popovic.fon.mywebproject.service.CityService;
import sasa.popovic.fon.mywebproject.service.StudentService;

@Controller
@RequestMapping(value = "/student")
@SessionAttributes({ "studentDto", "studentDtos", "cityDtos", "yearList" })
public class StudentController {

	@Autowired
	private StudentService studentService;
	@Autowired
	private CityService cityService;

	public StudentController(StudentService studentService) {
		this.studentService = studentService;
	}

	@GetMapping
	public ModelAndView home() {
		ModelAndView modelAndView = new ModelAndView("student");
		return modelAndView;
	}

	@GetMapping(value = "add")
	public ModelAndView add(SessionStatus sessionStatus) {
		sessionStatus.setComplete();
		ModelAndView modelAndView = new ModelAndView("student/studentAdd");
		return modelAndView;
	}

	@PostMapping(value = "confirm")
	public ModelAndView confirm(@Valid @ModelAttribute("studentDto") StudentDto studentDto,
//			Errors errors
			BindingResult result) {
		ModelAndView modelAndView = new ModelAndView();
		if (result.hasErrors()) {
			modelAndView.setViewName("student/studentAdd");
		} else {
			StudentDto student = studentService.findByindexNumber(studentDto.getIndexNumber());
			if (student == null) {
				student = studentService.findByEmail(studentDto.getEmail());
				if (student == null) {
					modelAndView.setViewName("student/studentConfirm");
				} else {
					modelAndView.setViewName("student/studentAdd");
					modelAndView.addObject("errorMailMessage", "Defined email already exists in database!");
				}
			} else {
				modelAndView.setViewName("student/studentAdd");
				modelAndView.addObject("errorIndexMessage", "Defined index already exists in database!");
			}

		}

		return modelAndView;
	}

	@PostMapping(value = "save")
	public ModelAndView save(@ModelAttribute("studentDto") StudentDto studentDto,
			@RequestParam(name = "submitForm") String submitForm, SessionStatus sessionStatus) {
		
		ModelAndView modelAndView = new ModelAndView("student/studentAdd");
		if (submitForm.equalsIgnoreCase("save")) {
			studentService.save(studentDto);
			sessionStatus.setComplete();
			modelAndView.setViewName("redirect:saved");
//			return "student/studentSaved";
		}
		if (submitForm.equalsIgnoreCase("cancel")) {
			sessionStatus.setComplete();
		}
		if (submitForm.equalsIgnoreCase("change")) {
		}

		return modelAndView;
	}
	
	@GetMapping(value = "saved")
	public ModelAndView saved() {
		return new ModelAndView("student/studentSaved");
	}
	

	@GetMapping("list")
	public ModelAndView list(@ModelAttribute("studentDtos") List<StudentDto> studentDtos) {
		ModelAndView modelAndView = new ModelAndView("student/studentList");
		return modelAndView;
	}

	@PostMapping(value = "edit")
	public ModelAndView edit(@RequestParam(name = "id") String id, SessionStatus sessionStatus) throws Exception {
		ModelAndView modelAndView = new ModelAndView("student/studentEdit");
		Long studentId = Long.parseLong(id);
		modelAndView.addObject("studentDto", studentService.findById(studentId));
		return modelAndView;
	}

	@PostMapping(value = "update")
	public ModelAndView update(@Valid @ModelAttribute("studentDto") StudentDto studentDto, SessionStatus sessionStatus,
			BindingResult result) {
		ModelAndView modelAndView = new ModelAndView("student/studentUpdated");
		System.out.println(studentDto);
		if (result.hasErrors()) {
			modelAndView.setViewName("student/studentEdit");
		} else {
			studentService.save(studentDto);
			sessionStatus.setComplete();
		}

		return modelAndView;
	}
	
	@ModelAttribute(name = "yearList")
	private Map<Long, String> generateYearList(){
		Map<Long, String> yearList = new HashMap<Long, String>();
		yearList.put(new Long(1), "First");
		yearList.put(new Long(2), "Second");
		yearList.put(new Long(3), "Third");
		yearList.put(new Long(4), "Forth");
		return yearList;
	}

	@ModelAttribute(name = "studentDto")
	private StudentDto generateStudentDto() {
		return new StudentDto();
	}

	@ModelAttribute(name = "studentDtos")
	private List<StudentDto> getAllStudents() {
		System.out.println("================== getAllStudents =========================");
		return studentService.getAll();
	}

	@ModelAttribute(name = "cityDtos")
	private List<CityDto> getAllCities() {
		System.out.println("===============getAllCities====================");
		return cityService.getAll();
	}

}
