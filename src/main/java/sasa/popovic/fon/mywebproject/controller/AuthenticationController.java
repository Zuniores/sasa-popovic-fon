package sasa.popovic.fon.mywebproject.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import sasa.popovic.fon.mywebproject.dto.EmployeeDto;
import sasa.popovic.fon.mywebproject.service.EmployeeService;
import sasa.popovic.fon.mywebproject.validator.EmployeeDtoValidator;

@Controller
@RequestMapping(value = "/authentication")
@SessionAttributes(value = "employeeDto")
public class AuthenticationController {

	@Autowired
	private EmployeeService employeeService;

	public AuthenticationController(EmployeeService employeeService) {
		this.employeeService = employeeService;
	}

	@GetMapping(value = "login")
	public ModelAndView login() {
		return new ModelAndView("authentication/login");
	}

	@PostMapping(value = "login")
	public ModelAndView authenticate(@Valid @ModelAttribute("employeeDto") EmployeeDto employeeDto, Errors errors,
			SessionStatus sessionStatus) {
		System.out.println("===============================================================");
		System.out.println("AuthenticationController: authenticate()=======================");
		System.out.println("===============================================================");

		ModelAndView modelAndView = new ModelAndView();
		if (errors.hasErrors()) {
			modelAndView.setViewName("authentication/login");
			sessionStatus.setComplete();
		} else {
			EmployeeDto employee = employeeService.findByUsername(employeeDto.getUsername());
			if (employee != null && employee.getPassword().equals(employeeDto.getPassword()))
				modelAndView.setViewName("index");
			else {
				modelAndView.setViewName("authentication/login");
				sessionStatus.setComplete();
				modelAndView.addObject("errorMessage", "There is no employee with provided credentials!");
			}
		}

		return modelAndView;

	}

	@GetMapping(value = "logout")
	public ModelAndView logout(SessionStatus sessionStatus) {
		sessionStatus.setComplete();
		return new ModelAndView("redirect:login");

	}

	@ModelAttribute(name = "employeeDto")
	private EmployeeDto generateEmployeeDto() {
		return new EmployeeDto();
	}

//	@InitBinder
//	public void initBinder(WebDataBinder binder) {
//		binder.addValidators(new EmployeeDtoValidator());
//	}

}
