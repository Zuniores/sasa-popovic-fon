package sasa.popovic.fon.mywebproject.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import sasa.popovic.fon.mywebproject.domain.Semester;
import sasa.popovic.fon.mywebproject.dto.SubjectDto;
import sasa.popovic.fon.mywebproject.service.SubjectService;

@Controller
@RequestMapping(value = "/subject")
@SessionAttributes({ "subjectDto", "subjectDtos" })
public class SubjectController {

	@Autowired
	private SubjectService subjectService;

	public SubjectController(SubjectService subjectService) {
		this.subjectService = subjectService;
	}

	@GetMapping
	public ModelAndView home() {
		ModelAndView modelAndView = new ModelAndView("subject");
		return modelAndView;
	}

	@GetMapping(value = "add")
	public ModelAndView add(SessionStatus sessionStatus) {
		sessionStatus.setComplete();
		ModelAndView modelAndView = new ModelAndView("subject/subjectAdd");
		 modelAndView.addObject("semesterValues", Semester.values());
		return modelAndView;
	}

	@PostMapping(value = "save")
	public ModelAndView confirm(
			@Valid @ModelAttribute("subjectDto") SubjectDto subjectDto,
			BindingResult result,
			SessionStatus sessionStatus) throws Exception {
		ModelAndView modelAndView = new ModelAndView();
		if (result.hasErrors()) {
			modelAndView.setViewName(("subject/subjectAdd"));
		} else {
			subjectService.save(subjectDto);
			sessionStatus.setComplete();
			modelAndView.setViewName("redirect:saved");
		}
		return modelAndView;
	}
	
	@GetMapping(value = "saved")
	public ModelAndView saved() {
		return new ModelAndView("subject/subjectSaved");
	}

	@GetMapping("list")
	public ModelAndView list(@ModelAttribute("subjectDtos") List<SubjectDto> subjectDtos) {
		ModelAndView modelAndView = new ModelAndView("subject/subjectList");
		return modelAndView;
	}

	@PostMapping(value = "edit")
	public ModelAndView edit(@RequestParam(name = "id") String id) throws Exception {
		Long subjectId = Long.parseLong(id);
		ModelAndView modelAndView = new ModelAndView("subject/subjectEdit");
		modelAndView.addObject("subjectDto", subjectService.findById(subjectId));
		return modelAndView;
	}

	@PostMapping(value = "update")
	public ModelAndView update(
			@Valid @ModelAttribute("subjectDto") SubjectDto subjectDto, 
			SessionStatus sessionStatus,
			BindingResult result) {
		ModelAndView modelAndView = new ModelAndView();
		System.out.println(subjectDto);
		if (result.hasErrors()) {
			modelAndView.setViewName("subject/subjectEdit");
		} else {
			subjectService.save(subjectDto);
			sessionStatus.setComplete();
			modelAndView.setViewName("redirect:updated");
		}
		return modelAndView;
	}
	
	@GetMapping(value = "updated")
	public ModelAndView updated() {
		return new ModelAndView("subject/subjectUpdated");
	}
	
	@ModelAttribute(name = "subjectDto")
	private SubjectDto generateSubjectDto() {
		return new SubjectDto();
	}
	
	@ModelAttribute(name = "subjectDtos")
	private List<SubjectDto> getAllSubjects() {
		System.out.println("================== getAllSubjects =========================");
		return subjectService.getAll();
	}

}
