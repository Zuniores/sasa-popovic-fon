package sasa.popovic.fon.mywebproject.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import sasa.popovic.fon.mywebproject.dto.ExamDto;
import sasa.popovic.fon.mywebproject.dto.ExamStudentDto;
import sasa.popovic.fon.mywebproject.dto.StudentDto;
import sasa.popovic.fon.mywebproject.service.ExamService;
import sasa.popovic.fon.mywebproject.service.ExamStudentService;
import sasa.popovic.fon.mywebproject.service.StudentService;

@Controller
@RequestMapping(value = "/examStudent")
@SessionAttributes({ "examStudentDto", "examStudentDtos","examDtos", "studentDtos"})
public class ExamStudentController {
	
	@Autowired
	private ExamStudentService examStudentService;
	@Autowired
	private ExamService examService;
	@Autowired
	private StudentService studentService;
	
	public ExamStudentController(
			ExamStudentService examStudentService,
			ExamService examService) {
		this.examStudentService = examStudentService;
		this.examService = examService;
	}
	
	@GetMapping
	public ModelAndView home() {
		ModelAndView modelAndView = new ModelAndView("examStudent");
		return modelAndView;
	}
	
	@GetMapping(value = "add")
	public ModelAndView add(SessionStatus sessionStatus) {
		sessionStatus.setComplete();
		ModelAndView modelAndView = new ModelAndView("examStudent/examStudentAdd");
		return modelAndView;
	}
	
	@PostMapping(value = "save")
	public ModelAndView confirm(
			@ModelAttribute("examStudentDto") ExamStudentDto examStudentDto,
			SessionStatus sessionStatus
	) {
		ModelAndView modelAndView = new ModelAndView();
		if(examStudentService.checkApplicatonCondition(examStudentDto)) {
			examStudentService.save(examStudentDto);
			sessionStatus.setComplete();
			modelAndView.setViewName("redirect:saved");
		}else {
			modelAndView.setViewName("examStudent/examStudentAdd");
			modelAndView.addObject("examStudentError", 
					"You can apply for exam only inside week before exam "
					+ "and you have to be on study year which is the same or one less then subjects lecture year!");
		}
		return modelAndView;
	}
	
	@GetMapping(value = "saved")
	public ModelAndView saved() {
		return new ModelAndView("examStudent/examStudentSaved");
	}
	
	
	@GetMapping("list")
	public ModelAndView list(@ModelAttribute("examStudentDtos") List<ExamStudentDto> examStudentDtos) {
		ModelAndView modelAndView = new ModelAndView("examStudent/examStudentList");
		return modelAndView;
	}
	
	@PostMapping(value = "edit")
	public ModelAndView edit(@RequestParam(name = "id") String id) throws Exception {
		Long examStudentId = Long.parseLong(id);
		ModelAndView modelAndView = new ModelAndView("examStudent/examStudentEdit");
		modelAndView.addObject("examStudentDto", examStudentService.findById(examStudentId));
		return modelAndView;
	}
	
	@PostMapping(value = "update")
	public ModelAndView update(@Valid @ModelAttribute("examStudentDto") ExamStudentDto examStudentDto, SessionStatus sessionStatus,
			BindingResult result) {
		ModelAndView modelAndView = new ModelAndView();
		System.out.println(examStudentDto);
		if (result.hasErrors()) {
			modelAndView.setViewName("examStudent/examStudentEdit");
		} else {
			examStudentService.save(examStudentDto);
			sessionStatus.setComplete();
			modelAndView.setViewName("redirect:updated");
		}
		return modelAndView;
	}
	
	@GetMapping(value = "updated")
	public ModelAndView updated() {
		return new ModelAndView("examStudent/examStudentUpdated");
	}
	
	
	@ModelAttribute(name = "examDtos")
	private List<ExamDto> getAllExams() {
		return examService.getAll();
	}
	
	@ModelAttribute(name = "examStudentDtos")
	private List<ExamStudentDto> getAllStudentExams() {
		return examStudentService.getAll();
	}
	
	@ModelAttribute(name = "examStudentDto")
	private ExamStudentDto generateExamStudentDto() {
		return new ExamStudentDto();
	}

	@ModelAttribute(name = "studentDtos")
	private List<StudentDto> getAllStudents() {
		return studentService.getAll();
	}
	
}
