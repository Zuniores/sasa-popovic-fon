package sasa.popovic.fon.mywebproject.controller;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import sasa.popovic.fon.mywebproject.dto.ExamDto;
import sasa.popovic.fon.mywebproject.dto.ProfessorDto;
import sasa.popovic.fon.mywebproject.dto.SubjectDto;
import sasa.popovic.fon.mywebproject.service.ExamService;
import sasa.popovic.fon.mywebproject.service.ProfessorService;
import sasa.popovic.fon.mywebproject.service.SubjectService;

@Controller
@RequestMapping(value = "/exam")
@SessionAttributes({ "examDto", "examDtos", "professorDtos", "subjectDtos" })
public class ExamController {

	@Autowired
	private ExamService examService;
	@Autowired
	private ProfessorService professorService;
	@Autowired
	private SubjectService subjectService;

	public ExamController(ExamService examService, ProfessorService professorService, SubjectService subjectService) {
		this.examService = examService;
		this.professorService = professorService;
		this.subjectService = subjectService;
	}

	@GetMapping
	public ModelAndView home() {
		ModelAndView modelAndView = new ModelAndView("exam");
		return modelAndView;
	}

	@GetMapping(value = "add")
	public ModelAndView add(SessionStatus sessionStatus) {
		sessionStatus.setComplete();
		ModelAndView modelAndView = new ModelAndView("exam/examAdd");

		return modelAndView;
	}

	@PostMapping(value = "save")
	public ModelAndView confirm(@Valid @ModelAttribute("examDto") ExamDto examDto, SessionStatus sessionStatus,
			BindingResult result) {
		ModelAndView modelAndView = new ModelAndView();
		if (result.hasErrors()) {
			modelAndView.setViewName("exam/examAdd");
		} else {
			if (examService.checkDatesCondition(examDto)) {
				examService.save(examDto);
				sessionStatus.setComplete();
				modelAndView.setViewName("redirect:saved");
			} else {
				modelAndView.setViewName("exam/examAdd");
				modelAndView.addObject("errorExam",
						"Datum novog ispita mozete uneti tek dan nakon odrzavanja ispita. Datum ne sme biti pre poslednje odrzanog!");
			}
		}
		return modelAndView;
	}
	
	@GetMapping(value = "saved")
	public ModelAndView saved() {
		return new ModelAndView("exam/examSaved");
	}

	@PostMapping(value = "edit")
	public ModelAndView edit(@RequestParam(name = "id") String id) throws Exception {
		Long examId = Long.parseLong(id);
		ModelAndView modelAndView = new ModelAndView("exam/examEdit");
		modelAndView.addObject("examDto", examService.findById(examId));
		return modelAndView;
	}
//
//	@ModelAttribute("professorDto")
//	public ProfessorDto populateExamDto(@RequestParam(name = "professorDto") Long id) throws Exception {
//	    return professorService.findById(id);
//	}
	
	@PostMapping(value = "update")
	public ModelAndView update(@Valid @ModelAttribute("examDto") ExamDto examDto, SessionStatus sessionStatus,
			BindingResult result) {
		ModelAndView modelAndView = new ModelAndView();
		System.out.println(examDto);
		if (result.hasErrors()) {
			modelAndView.setViewName("exam/examEdit");
		} else {
			examService.save(examDto);
			sessionStatus.setComplete();
			modelAndView.setViewName("redirect:updated");
		}
		return modelAndView;
	}
	
//	@PostMapping(value = "update")
//	public ModelAndView update(
////			@RequestParam(value = "profAndSubj", required=false) List<String> listId, 
//			@RequestParam("id") String id,
////			@RequestParam("professorDto") Long professorId,
////			@RequestParam("subjectDto") Long subjectId,
////			@RequestParam("date") Date date, 
//			SessionStatus sessionStatus
//			) throws Exception {
//		ModelAndView modelAndView = new ModelAndView();
////		for (String long1 : listId) {
////			System.out.println("param" + long1);
////		}
////		System.out.println(date);
////		ExamDto examDto = examService.findById(listId.get(0));
////		ProfessorDto professorDto = professorService.findById(professorId);
////		SubjectDto subjectDto = subjectService.findById(subjectId);
////		ExamDto examDto = new ExamDto(examId, professorDto, subjectDto, date);
////		System.out.println(examDto);
////			examService.save(examDto);
//			sessionStatus.setComplete();
//			modelAndView.setViewName("redirect:updated");
//		return modelAndView;
//	}
	
	@GetMapping(value = "updated")
	public ModelAndView updated() {
		return new ModelAndView("exam/examUpdated");
	}

	@GetMapping("list")
	public ModelAndView list(@ModelAttribute("examDtos") List<ExamDto> examDtos) {
		ModelAndView modelAndView = new ModelAndView("exam/examList");
		return modelAndView;
	}

	@ModelAttribute(name = "examDto")
	private ExamDto generateExamDto() {
		return new ExamDto();
	}

	@ModelAttribute(name = "examDtos")
	private List<ExamDto> getAllExams() {
		return examService.getAll();
	}

	@ModelAttribute(name = "professorDtos")
	private List<ProfessorDto> getAllProfessors() {
		return professorService.getAll();
	}

	@ModelAttribute(name = "subjectDtos")
	private List<SubjectDto> getAllSubjects() {
		return subjectService.getAll();
	}

}
