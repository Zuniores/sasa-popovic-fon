package sasa.popovic.fon.mywebproject.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import sasa.popovic.fon.mywebproject.dto.CityDto;
import sasa.popovic.fon.mywebproject.service.CityService;

@Controller
@RequestMapping(value = "/city")
@SessionAttributes({ "cityDto", "cityDtos" })
public class CityController {

	@Autowired
	private CityService cityService;

	public CityController(CityService cityService) {
		this.cityService = cityService;
	}

	@GetMapping
	public ModelAndView home() {
		ModelAndView modelAndView = new ModelAndView("city");
		return modelAndView;
	}

	@GetMapping(value = "add")
	public ModelAndView add(SessionStatus sessionStatus) {
		sessionStatus.setComplete();
		ModelAndView modelAndView = new ModelAndView("city/cityAdd");
		return modelAndView;
	}

	@PostMapping(value = "save")
	public ModelAndView confirm(@Valid @ModelAttribute("cityDto") CityDto cityDto,
//			Errors errors
			BindingResult result, SessionStatus sessionStatus) throws Exception {
		ModelAndView modelAndView = new ModelAndView();
		if (result.hasErrors()) {
			modelAndView.setViewName("city/cityAdd");
		} else {
			CityDto city = cityService.findByNumber(cityDto.getNumber());
			if (city == null) {
				cityService.save(cityDto);
				sessionStatus.setComplete();
				modelAndView.setViewName("redirect:saved");
			} else {
				modelAndView.addObject("errorMessage", "Defined number already exists in database!");
				modelAndView.setViewName("city/cityAdd");
			}

		}
		return modelAndView;

	}
	
	@GetMapping(value = "saved")
	public ModelAndView saved() {
		return new ModelAndView("city/citySaved");
	}

	@GetMapping("list")
	public ModelAndView list(@ModelAttribute("cityDtos") List<CityDto> cityDtos) {
		ModelAndView modelAndView = new ModelAndView("city/cityList");
		return modelAndView;
	}

	@PostMapping(value = "edit")
	public ModelAndView edit(@RequestParam(name = "id") String id) throws Exception {
		Long cityId = Long.parseLong(id);
		ModelAndView modelAndView = new ModelAndView("city/cityEdit");
		modelAndView.addObject("cityDto", cityService.findById(cityId));
		return modelAndView;
	}

	@PostMapping(value = "update")
	public ModelAndView update(@Valid @ModelAttribute("cityDto") CityDto cityDto, SessionStatus sessionStatus,
			BindingResult result) {
		ModelAndView modelAndView = new ModelAndView();
		System.out.println(cityDto);
		if (result.hasErrors()) {
			modelAndView.setViewName("city/cityEdit");
		} else {
			CityDto city = cityService.findByNumber(cityDto.getNumber());
			if (city == null) {
				cityService.save(cityDto);
				sessionStatus.setComplete();
				modelAndView.setViewName("redirect:updated");
			} else {
				modelAndView.addObject("errorMessage", "Defined id already exists in database!");
				modelAndView.setViewName("city/cityAdd");
			}
		}
		return modelAndView;
	}
	
	@GetMapping(value = "updated")
	public ModelAndView updated() {
		return new ModelAndView("city/cityUpdated");
	}

	@ModelAttribute(name = "cityDto")
	private CityDto generateCityDto() {
		return new CityDto();
	}

	@ModelAttribute(name = "cityDtos")
	private List<CityDto> getAllCities() {
		return cityService.getAll();
	}

}
