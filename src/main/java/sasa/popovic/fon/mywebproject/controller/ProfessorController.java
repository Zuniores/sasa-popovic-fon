package sasa.popovic.fon.mywebproject.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import sasa.popovic.fon.mywebproject.dto.CityDto;
import sasa.popovic.fon.mywebproject.dto.ProfessorDto;
import sasa.popovic.fon.mywebproject.dto.TitleDto;
import sasa.popovic.fon.mywebproject.exception.ProfessorEmailValidationException;
import sasa.popovic.fon.mywebproject.service.CityService;
import sasa.popovic.fon.mywebproject.service.ProfessorService;
import sasa.popovic.fon.mywebproject.service.TitleService;

@Controller
@RequestMapping(value = "/professor")
@SessionAttributes({ "professorDto", "professorDtos", "cityDtos", "titleDtos"})
public class ProfessorController {

	@Autowired
	private ProfessorService professorService;
	@Autowired
	private CityService cityService;
	@Autowired
	private TitleService titleService;

	public ProfessorController(
			ProfessorService professorService,
			CityService cityService,
			TitleService titleService) 
	{
		this.professorService = professorService;
		this.cityService = cityService;
		this.titleService = titleService;
	}

	@GetMapping
	public ModelAndView home() {
		ModelAndView modelAndView = new ModelAndView("professor");
		return modelAndView;
	}

	@GetMapping(value = "add")
	public ModelAndView add(SessionStatus sessionStatus) {
		sessionStatus.setComplete();
		ModelAndView modelAndView = new ModelAndView("professor/professorAdd");
		return modelAndView;
	}

	@PostMapping(value = "confirm")
	public ModelAndView confirm(@Valid @ModelAttribute("professorDto") ProfessorDto professorDto,
			BindingResult result) throws Exception {
		ModelAndView modelAndView = new ModelAndView();
		if (result.hasErrors()) {
			modelAndView.setViewName("professor/professorAdd");
		} else {
			if (professorService.findByEmail(professorDto.getEmail()) == null) {
				modelAndView.setViewName("professor/professorConfirm");
			} else {
				modelAndView.addObject("errorMessage", "Defined email already exists in database!");
				modelAndView.setViewName("professor/professorAdd");
			}

		}

		return modelAndView;
	}

	@PostMapping(value = "save")
	public ModelAndView save(
			@ModelAttribute("professorDto") ProfessorDto professorDto,
			@RequestParam(name = "submitForm") String submitForm,
			SessionStatus sessionStatus) throws ProfessorEmailValidationException {
		
		ModelAndView modelAndView = new ModelAndView("professor/professorAdd");
		if (submitForm.equalsIgnoreCase("save")) {
			System.out.println(professorDto.getEmail());
//			professorService.checkEmail(professorDto.getEmail());
			professorService.save(professorDto);
			sessionStatus.setComplete();
			modelAndView.setViewName("redirect:saved");
		}
		if (submitForm.equalsIgnoreCase("cancel")) {
			sessionStatus.setComplete();
		}
		if (submitForm.equalsIgnoreCase("change")) {

		}

//		return "redirect:/professor/add";
		return modelAndView;
	}
	
	@GetMapping(value = "saved")
	public ModelAndView saved() {
		return new ModelAndView("professor/professorSaved");
	}

	@GetMapping("list")
	public ModelAndView list(@ModelAttribute("professorDtos") List<ProfessorDto> professorDtos) {
		ModelAndView modelAndView = new ModelAndView("professor/professorList");
		return modelAndView;
	}
	
	@PostMapping(value = "edit")
	public ModelAndView edit(
			@RequestParam(name = "id") String id
			
			) throws Exception {
		ModelAndView modelAndView = new ModelAndView("professor/professorEdit");
		Long professorId = Long.parseLong(id);
		modelAndView.addObject("professorDto", professorService.findById(professorId));
		return modelAndView;
	}
	
	@PostMapping(value = "update")
	public ModelAndView update(
			@Valid @ModelAttribute("professorDto") ProfessorDto professorDto,
			SessionStatus sessionStatus,
			BindingResult result
			) throws ProfessorEmailValidationException {
		System.out.println(professorDto.getFirstname());
		ModelAndView modelAndView = new ModelAndView();
		if (result.hasErrors()) {
			modelAndView.setViewName("professor/professorEdit");
		}else {
			ProfessorDto professor = professorService.findByEmail(professorDto.getEmail());
			if (professor != null && professor.getId().equals(professorDto.getId())) {
				professorService.save(professorDto);
				sessionStatus.setComplete();
				modelAndView.setViewName("redirect:updated");
				
			}else {
				modelAndView.addObject("errorMessage", "Defined email already exists at another user!");
				modelAndView.setViewName("professor/professorEdit");
			}
			
		}
		return modelAndView;
	}
	
	@GetMapping(value = "updated")
	public ModelAndView updated() {
		return new ModelAndView("professor/professorUpdated");
	}

	@ModelAttribute(name = "professorDto")
	private ProfessorDto generateProfessorDto() {
		return new ProfessorDto();
	}

	@ModelAttribute(name = "professorDtos")
	private List<ProfessorDto> getAllProfessors() {
		System.out.println("================== getAllProfessors =========================");
		return professorService.getAll();
	}

	@ModelAttribute(name = "cityDtos")
	private List<CityDto> getAllCities() {
		System.out.println("===============getAllCities====================");
		return cityService.getAll();
	}
	
	@ModelAttribute(name = "titleDtos")
	private List<TitleDto> getAllTitles() {
		System.out.println("===============getAllTitles====================");
		return titleService.getAll();
	}
	
	@ExceptionHandler(ProfessorEmailValidationException.class)
	public ModelAndView exceptionHandler(ProfessorEmailValidationException professorEmailValidationException) {
		System.out.println("====================================================================");
		System.out.println("@ExceptionHandler exception ocured: ProfessorEmailValidationException");
		System.out.println("====================================================================");
		ModelAndView modelAndView = new ModelAndView("professor/professorAdd");
		modelAndView.addObject("errorMessage", professorEmailValidationException.getMessage());
		return modelAndView;
	}

//	
//	@InitBinder
//	public void initBinder(WebDataBinder binder) {
//		binder.addValidators(new ProfessorDtoValidator());
//	}
}
