package sasa.popovic.fon.mywebproject.dto;

import java.io.Serializable;
import java.util.Set;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import sasa.popovic.fon.mywebproject.domain.Exam;
import sasa.popovic.fon.mywebproject.domain.Semester;

public class SubjectDto implements Serializable {
	private static final long serialVersionUID = -5484815697736935694L;

	private Long id;
	@NotEmpty(message = "Please input firstname")
	@Size(min = 3, max = 30, message = "Name can be between 3 and 30 characters")
	private String name;
	@Size(max = 200, message = "Description can be up to 200 characters")
	private String description;
	private int yearOfStudy;
	private Semester semester;

//	private Set<Exam> exam;

	public SubjectDto() {

	}

	public SubjectDto(String name, String description, int yearOfStudy, Semester semester) {
		this.name = name;
		this.description = description;
		this.yearOfStudy = yearOfStudy;
		this.semester = semester;
//		this.exam = exam;
	}

	public SubjectDto(Long id, String name, String description, int yearOfStudy, Semester semester) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.yearOfStudy = yearOfStudy;
		this.semester = semester;
//		this.exam = exam;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getYearOfStudy() {
		return yearOfStudy;
	}

	public void setYearOfStudy(int yearOfStudy) {
		this.yearOfStudy = yearOfStudy;
	}

	public Semester getSemester() {
		return semester;
	}

	public void setSemester(Semester semester) {
		this.semester = semester;
	}

//	public Set<Exam> getExam() {
//		return exam;
//	}
//
//	public void setExam(Set<Exam> exam) {
//		this.exam = exam;
//	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((semester == null) ? 0 : semester.hashCode());
		result = prime * result + yearOfStudy;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubjectDto other = (SubjectDto) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (semester != other.semester)
			return false;
		if (yearOfStudy != other.yearOfStudy)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return name;
	}

}
