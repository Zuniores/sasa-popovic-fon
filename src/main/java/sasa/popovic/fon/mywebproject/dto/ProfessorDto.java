package sasa.popovic.fon.mywebproject.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import sasa.popovic.fon.mywebproject.domain.Exam;

public class ProfessorDto implements Serializable {
	private static final long serialVersionUID = -7647203886985881874L;

	private Long id;
	@NotEmpty(message = "Please input firstname")
	@Size(min = 3, max = 30, message = "First name can be between 3 and 30 characters")
	private String firstname;
	@NotEmpty(message = "Please input lastname")
	@Size(min = 3, max = 30, message = "Last name can be between 3 and 30 characters")
	private String lastname;
	@Email(message = "Email is not valid")
	private String email;
	@Size(min = 3, max = 50, message = "Address can be between 3 and 50 characters")
	private String address;
	private CityDto cityDto;
//	@Pattern(regexp="\\(\\d{3}\\)\\d{3}-\\d{4}", message = "Please input data in format(XXX)XXX-XXXX")
	@Size(min = 6, max = 15, message = "Phone number can be between 6 and 15 characters")
	private String phone;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
//	@NotEmpty(message = "Please select date")
	private Date reelectionDate;
	private TitleDto titleDto;
	private List<ExamDto> exams;

	public ProfessorDto() {

	}

	public ProfessorDto(
			@NotEmpty(message = "Please input firstname") @Size(min = 3, max = 30, message = "First name can be between 3 and 30 characters") String firstname,
			@NotEmpty(message = "Please input lastname") @Size(min = 3, max = 30, message = "Last name can be between 3 and 30 characters") String lastname,
			@Email(message = "Email is not valid") String email,
			@Size(min = 3, max = 50, message = "Address can be between 3 and 50 characters") String address,
			CityDto cityDto,
			@Size(min = 6, max = 15, message = "Phone number can be between 6 and 15 characters") String phone,
			Date reelectionDate, TitleDto titleDto, List<ExamDto> exam) {
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.address = address;
		this.cityDto = cityDto;
		this.phone = phone;
		this.reelectionDate = reelectionDate;
		this.titleDto = titleDto;
		this.exams = exam;
	}

	public ProfessorDto(Long id,
			@NotEmpty(message = "Please input firstname") @Size(min = 3, max = 30, message = "First name can be between 3 and 30 characters") String firstname,
			@NotEmpty(message = "Please input lastname") @Size(min = 3, max = 30, message = "Last name can be between 3 and 30 characters") String lastname,
			@Email(message = "Email is not valid") String email,
			@Size(min = 3, max = 50, message = "Address can be between 3 and 50 characters") String address,
			CityDto cityDto,
			@Size(min = 6, max = 15, message = "Phone number can be between 6 and 15 characters") String phone,
			Date reelectionDate, TitleDto titleDto, List<ExamDto> exam) {
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.address = address;
		this.cityDto = cityDto;
		this.phone = phone;
		this.reelectionDate = reelectionDate;
		this.titleDto = titleDto;
		this.exams = exam;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public CityDto getCityDto() {
		return cityDto;
	}

	public void setCityDto(CityDto cityDto) {
		this.cityDto = cityDto;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getReelectionDate() {
		return reelectionDate;
	}

	public void setReelectionDate(Date reelectionDate) {
		this.reelectionDate = reelectionDate;
	}

	public TitleDto getTitleDto() {
		return titleDto;
	}

	public void setTitleDto(TitleDto titleDto) {
		this.titleDto = titleDto;
	}

	public List<ExamDto> getExamDtos() {
		return exams;
	}

	public void setExamDtos(List<ExamDto> exam) {
		this.exams = exam;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((firstname == null) ? 0 : firstname.hashCode());
		result = prime * result + ((lastname == null) ? 0 : lastname.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProfessorDto other = (ProfessorDto) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstname == null) {
			if (other.firstname != null)
				return false;
		} else if (!firstname.equals(other.firstname))
			return false;
		if (lastname == null) {
			if (other.lastname != null)
				return false;
		} else if (!lastname.equals(other.lastname))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return firstname + " " + lastname;
//		return "ProfessorDto [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", email=" + email
//				+ ", address=" + address + ", cityDto=" + cityDto + ", phone=" + phone + ", reelectionDate="
//				+ reelectionDate + ", titleDto=" + titleDto + "]";
	}

}
