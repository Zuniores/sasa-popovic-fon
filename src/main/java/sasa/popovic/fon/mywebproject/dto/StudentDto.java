package sasa.popovic.fon.mywebproject.dto;

import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class StudentDto implements Serializable {
	private static final long serialVersionUID = 1726100922163119863L;

	private Long id;

	@NotEmpty(message = "Please input index number")
	@Size(min = 10, max = 10, message = "Index number must be 10 characters")
	private String indexNumber;
	@NotEmpty(message = "Please input firstname")
	@Size(min = 3, max = 30, message = "First name can be between 3 and 30 characters")
	private String firstname;
	@NotEmpty(message = "Please input lastname")
	@Size(min = 3, max = 30, message = "Last name can be between 3 and 30 characters")
	private String lastname;
	@Email(message = "Email is not valid")
	private String email;
	@Size(min = 3, max = 50, message = "Address can be between 3 and 50 characters")
	private String address;
	private CityDto cityDto;
//	@Pattern(regexp="\\(\\d{3}\\)\\d{3}-\\d{4}")
	@Size(min = 6, max = 15, message = "Phone number can be between 6 and 15 characters")
	private String phone;
	private Long currentYearOfStudy;

	public StudentDto() {

	}

	public StudentDto(String indexNumber, String firstname, String lastname, String email, String address,
			CityDto cityDto, String phone, Long currentYearOfStudy) {
		this.indexNumber = indexNumber;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.address = address;
		this.cityDto = cityDto;
		this.phone = phone;
		this.currentYearOfStudy = currentYearOfStudy;
	}

	public StudentDto(Long id, String indexNumber, String firstname, String lastname, String email, String address,
			CityDto cityDto, String phone, Long currentYearOfStudy) {
		this.id = id;
		this.indexNumber = indexNumber;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.address = address;
		this.cityDto = cityDto;
		this.phone = phone;
		this.currentYearOfStudy = currentYearOfStudy;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIndexNumber() {
		return indexNumber;
	}

	public void setIndexNumber(String indexNumber) {
		this.indexNumber = indexNumber;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public CityDto getCityDto() {
		return cityDto;
	}

	public void setCityDto(CityDto cityDto) {
		this.cityDto = cityDto;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Long getCurrentYearOfStudy() {
		return currentYearOfStudy;
	}

	public void setCurrentYearOfStudy(Long currentYearOfStudy) {
		this.currentYearOfStudy = currentYearOfStudy;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((cityDto == null) ? 0 : cityDto.hashCode());
		result = prime * result + ((currentYearOfStudy == null) ? 0 : currentYearOfStudy.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((firstname == null) ? 0 : firstname.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((indexNumber == null) ? 0 : indexNumber.hashCode());
		result = prime * result + ((lastname == null) ? 0 : lastname.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StudentDto other = (StudentDto) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (cityDto == null) {
			if (other.cityDto != null)
				return false;
		} else if (!cityDto.equals(other.cityDto))
			return false;
		if (currentYearOfStudy == null) {
			if (other.currentYearOfStudy != null)
				return false;
		} else if (!currentYearOfStudy.equals(other.currentYearOfStudy))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstname == null) {
			if (other.firstname != null)
				return false;
		} else if (!firstname.equals(other.firstname))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (indexNumber == null) {
			if (other.indexNumber != null)
				return false;
		} else if (!indexNumber.equals(other.indexNumber))
			return false;
		if (lastname == null) {
			if (other.lastname != null)
				return false;
		} else if (!lastname.equals(other.lastname))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return indexNumber + ": " + firstname + " " + lastname;
//		return "StudentDto [id=" + id + ", indexNumber=" + indexNumber + ", firstname=" + firstname + ", lastname="
//				+ lastname + ", email=" + email + ", address=" + address + ", cityDto=" + cityDto + ", phone=" + phone
//				+ ", currentYearOfStudy=" + currentYearOfStudy + "]";
	}

}