package sasa.popovic.fon.mywebproject.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotEmpty;

import org.springframework.format.annotation.DateTimeFormat;

public class ExamStudentDto implements Serializable{
	private static final long serialVersionUID = -1651092771590475308L;
	
	private Long id;
	private StudentDto studentDto;
	private ExamDto examDto;
//	@NotEmpty(message = "Please select date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date enrollDate;

	public ExamStudentDto() {
		
	}

	public ExamStudentDto(StudentDto studentDto, ExamDto examDto, Date enrollDate) {
		this.studentDto = studentDto;
		this.examDto = examDto;
		this.enrollDate = enrollDate;
	}

	public ExamStudentDto(Long id, StudentDto studentDto, ExamDto examDto, Date enrollDate) {
		this.id = id;
		this.studentDto = studentDto;
		this.examDto = examDto;
		this.enrollDate = enrollDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public StudentDto getStudentDto() {
		return studentDto;
	}

	public void setStudentDto(StudentDto studentDto) {
		this.studentDto = studentDto;
	}

	public ExamDto getExamDto() {
		return examDto;
	}

	public void setExamDto(ExamDto examDto) {
		this.examDto = examDto;
	}

	public Date getEnrollDate() {
		return enrollDate;
	}

	public void setEnrollDate(Date enrollDate) {
		this.enrollDate = enrollDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((enrollDate == null) ? 0 : enrollDate.hashCode());
		result = prime * result + ((examDto == null) ? 0 : examDto.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((studentDto == null) ? 0 : studentDto.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExamStudentDto other = (ExamStudentDto) obj;
		if (enrollDate == null) {
			if (other.enrollDate != null)
				return false;
		} else if (!enrollDate.equals(other.enrollDate))
			return false;
		if (examDto == null) {
			if (other.examDto != null)
				return false;
		} else if (!examDto.equals(other.examDto))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (studentDto == null) {
			if (other.studentDto != null)
				return false;
		} else if (!studentDto.equals(other.studentDto))
			return false;
		return true;
	}
	
	
	
}

