package sasa.popovic.fon.mywebproject.dto;

import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class ExamDto implements Serializable {
	private static final long serialVersionUID = -669875684887825428L;

	private Long id;
	private ProfessorDto professorDto;
	private SubjectDto subjectDto;
//	@NotNull(message = "Please select date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date date;

	public ExamDto() {

	}

	public ExamDto(ProfessorDto professorDto, SubjectDto subjectDto, Date date) {
		this.professorDto = professorDto;
		this.subjectDto = subjectDto;
		this.date = date;
	}

	public ExamDto(Long id, ProfessorDto professorDto, SubjectDto subjectDto, Date date) {
		this.id = id;
		this.professorDto = professorDto;
		this.subjectDto = subjectDto;
		this.date = date;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ProfessorDto getProfessorDto() {
		return professorDto;
	}

	public void setProfessorDto(ProfessorDto professorDto) {
		this.professorDto = professorDto;
	}

	public SubjectDto getSubjectDto() {
		return subjectDto;
	}

	public void setSubjectDto(SubjectDto subjectDto) {
		this.subjectDto = subjectDto;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((professorDto == null) ? 0 : professorDto.hashCode());
		result = prime * result + ((subjectDto == null) ? 0 : subjectDto.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExamDto other = (ExamDto) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (professorDto == null) {
			if (other.professorDto != null)
				return false;
		} else if (!professorDto.equals(other.professorDto))
			return false;
		if (subjectDto == null) {
			if (other.subjectDto != null)
				return false;
		} else if (!subjectDto.equals(other.subjectDto))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return subjectDto + " - " + date + " - Year of study: " + subjectDto.getYearOfStudy();
	}

}