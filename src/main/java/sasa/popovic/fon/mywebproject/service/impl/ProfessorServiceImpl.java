package sasa.popovic.fon.mywebproject.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sasa.popovic.fon.mywebproject.converter.ProfessorConverter;
import sasa.popovic.fon.mywebproject.domain.City;
import sasa.popovic.fon.mywebproject.domain.Professor;
import sasa.popovic.fon.mywebproject.dto.ProfessorDto;
import sasa.popovic.fon.mywebproject.exception.ProfessorEmailValidationException;
import sasa.popovic.fon.mywebproject.repository.CityRepository;
import sasa.popovic.fon.mywebproject.repository.ProfessorRepository;
import sasa.popovic.fon.mywebproject.service.ProfessorService;

@Service
@Transactional
public class ProfessorServiceImpl implements ProfessorService {
	@Autowired
	private ProfessorRepository professorRepository;
	@Autowired
	private CityRepository cityRepository;
	@Autowired
	private ProfessorConverter professorConverter;

	public ProfessorServiceImpl(ProfessorRepository professorRepository, ProfessorConverter professorConverter) {
		this.professorRepository = professorRepository;
		this.professorConverter = professorConverter;
	}

	@Override
	public void save(ProfessorDto professorDto) throws ProfessorEmailValidationException {
		Professor professor;
		if (professorDto.getId() == null) {
			professor = professorConverter.dtoToEntity(professorDto);
		} else {
			
			professor = professorConverter.dtoToEntityWithId(professorDto);
		}
		City city = cityRepository.findById(professorDto.getCityDto().getId()).get();
		professor.setCity(city);
		professorRepository.save(professor);
	}

	@Override
	public ProfessorDto findById(Long id) throws Exception {
		Optional<Professor> professor = professorRepository.findById(id);
		if (professor.isPresent()) {
			return professorConverter.entityToDtoWithId(professor.get());
		}
		throw new Exception("Professor does not exist");
	}

	@Override
	public List<ProfessorDto> getAll() {
		List<Professor> professors = professorRepository.findAll();
		List<ProfessorDto> professorDtos = new ArrayList<ProfessorDto>();
		for (Professor professor : professors) {
			professorDtos.add(professorConverter.entityToDtoWithId(professor));
		}
		return professorDtos;
	}

	@Override
	public ProfessorDto findByEmail(String email){
		List<Professor> professors = professorRepository.findByEmail(email);
		if (professors.size() != 0) {
			return professorConverter.entityToDtoWithId(professors.get(0));
		} else
			return null;
	}
	
	public void checkEmail(String email) throws ProfessorEmailValidationException {
		if (professorRepository.findByEmail(email).size() != 0)
			throw new ProfessorEmailValidationException("Defined email already exists in database!");
	}

}
