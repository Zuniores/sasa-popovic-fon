package sasa.popovic.fon.mywebproject.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sasa.popovic.fon.mywebproject.converter.TitleConverter;
import sasa.popovic.fon.mywebproject.domain.Title;
import sasa.popovic.fon.mywebproject.dto.TitleDto;
import sasa.popovic.fon.mywebproject.repository.TitleRepository;
import sasa.popovic.fon.mywebproject.service.TitleService;

@Service
@Transactional
public class TitleServiceImpl implements TitleService {
	@Autowired
	private TitleRepository titleRepository;

	@Autowired
	private TitleConverter titleConverter;

	public TitleServiceImpl(TitleRepository titleRepository) {
		this.titleRepository = titleRepository;
	}

	@Override
	public void save(TitleDto titleDto) {
		if(titleDto.getId() == null)
			titleRepository.save(titleConverter.dtoToEntity(titleDto));
		else
			titleRepository.save(titleConverter.dtoToEntityWithId(titleDto));

	}

	@Override
	public TitleDto findById(Long id) throws Exception {
		Optional<Title> title = titleRepository.findById(id);
		if (title.isPresent())
			return titleConverter.entityToDtoWithId(title.get());
		throw new Exception("Title does not exist");
	}

	@Override
	public List<TitleDto> getAll() {
		List<Title> titles = titleRepository.findAll();
		List<TitleDto> titleDtos = new ArrayList<TitleDto>();
		for (Title title : titles) {
			titleDtos.add(titleConverter.entityToDtoWithId(title));
		}
		return titleDtos;
	}
}
