package sasa.popovic.fon.mywebproject.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sasa.popovic.fon.mywebproject.converter.ExamStudentConverter;
import sasa.popovic.fon.mywebproject.domain.Exam;
import sasa.popovic.fon.mywebproject.domain.ExamStudent;
import sasa.popovic.fon.mywebproject.domain.Student;
import sasa.popovic.fon.mywebproject.dto.ExamDto;
import sasa.popovic.fon.mywebproject.dto.ExamStudentDto;
import sasa.popovic.fon.mywebproject.repository.ExamRepository;
import sasa.popovic.fon.mywebproject.repository.ExamStudentRepository;
import sasa.popovic.fon.mywebproject.repository.StudentRepository;
import sasa.popovic.fon.mywebproject.service.ExamStudentService;

@Service
@Transactional
public class ExamStudentServiceImpl implements ExamStudentService {

	@Autowired
	private ExamStudentRepository examStudentRepository;
	@Autowired
	private ExamRepository examRepository;
	@Autowired
	private StudentRepository studentRepository;
	@Autowired
	private ExamStudentConverter examStudentConverter;

	@Override
	public void save(ExamStudentDto examStudentDto) {
		ExamStudent examStudent;
		if (examStudentDto.getId() == null) {
			examStudent = examStudentConverter.dtoToEntity(examStudentDto);
		} else {
			examStudent = examStudentConverter.dtoToEntityWithId(examStudentDto);
		}
		Student student = studentRepository.findById(examStudentDto.getStudentDto().getId()).get();
		
		Exam exam = examRepository.findById(examStudentDto.getExamDto().getId()).get();
		
		examStudent.setExam(exam);
		examStudent.setStudent(student);
		
		examStudentRepository.save(examStudent);
	}

	@Override
	public ExamStudentDto findById(Long id) throws Exception {
		Optional<ExamStudent> examStudent = examStudentRepository.findById(id);
		if(examStudent.isPresent()) {
			return examStudentConverter.entityToDtoWithId(examStudent.get());
		}
		throw new Exception("Exam application does not exist");
	}

	@Override
	public List<ExamStudentDto> getAll() {
		List<ExamStudentDto> examStudentDtos = new ArrayList<ExamStudentDto>();
		List<ExamStudent> examStudents = examStudentRepository.findAll();
		for (ExamStudent examStudent : examStudents) {
			examStudentDtos.add(examStudentConverter.entityToDtoWithId(examStudent));
		}
		return examStudentDtos;
	}

	@Override
	public boolean checkApplicatonCondition(ExamStudentDto examStudentDto) {
		
		List<ExamStudentDto> examStudentDtos = getAll();
		for (ExamStudentDto examStudent : examStudentDtos) {
			//check if relation student - exam is unique
			if (examStudent.getStudentDto().equals(examStudentDto.getStudentDto())
					&& examStudent.getExamDto().equals(examStudentDto.getExamDto())) 
				return false;
		}
		
		//getting exam date
		Date dateOfExam = examStudentDto.getExamDto().getDate();
		//setting date a week before
		Calendar c = Calendar.getInstance();
		c.setTime(dateOfExam);
		c.add(Calendar.DATE, -7);
		Date weekBeforeDate = c.getTime();
		//getting todays date
		Date today = new Date();
		// checking if todays date is between exam date and week before
		if (today.after(weekBeforeDate) && today.before(dateOfExam)) {
			Long studentCurrentYearOfStudy = examStudentDto.getStudentDto().getCurrentYearOfStudy();
			int subjectYearOfStudy = examStudentDto.getExamDto().getSubjectDto().getYearOfStudy();
			if(studentCurrentYearOfStudy == subjectYearOfStudy || studentCurrentYearOfStudy == (subjectYearOfStudy - 1))
				return true;
			else
				return false;
		} else {
			return false;
		}
	}

}
