package sasa.popovic.fon.mywebproject.service;

import java.util.List;

import sasa.popovic.fon.mywebproject.dto.ExamDto;

public interface ExamService {
	void save(ExamDto examDto);
	ExamDto findById(Long id) throws Exception;
	List<ExamDto> getAll();
	boolean checkDatesCondition(ExamDto examDto);
}
