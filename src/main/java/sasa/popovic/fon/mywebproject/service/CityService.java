package sasa.popovic.fon.mywebproject.service;

import java.util.List;

import sasa.popovic.fon.mywebproject.dto.CityDto;

public interface CityService {
	void save(CityDto cityDto);
	CityDto findById(Long id) throws Exception;
	CityDto findByNumber(Long number);
	List<CityDto> getAll();
}
