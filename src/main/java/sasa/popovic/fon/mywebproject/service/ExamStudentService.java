package sasa.popovic.fon.mywebproject.service;

import java.util.List;

import sasa.popovic.fon.mywebproject.dto.ExamStudentDto;

public interface ExamStudentService {
	void save(ExamStudentDto examStudent);
	ExamStudentDto findById(Long id) throws Exception;
	List<ExamStudentDto> getAll();
	boolean checkApplicatonCondition(ExamStudentDto examStudentDto);
}
