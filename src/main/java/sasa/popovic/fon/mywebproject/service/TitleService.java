package sasa.popovic.fon.mywebproject.service;

import java.util.List;

import sasa.popovic.fon.mywebproject.dto.TitleDto;

public interface TitleService {
	void save(TitleDto titleDto);
	TitleDto findById(Long id) throws Exception;
	List<TitleDto> getAll();
}
