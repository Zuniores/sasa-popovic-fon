package sasa.popovic.fon.mywebproject.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sasa.popovic.fon.mywebproject.converter.EmployeeConverter;
import sasa.popovic.fon.mywebproject.domain.Employee;
import sasa.popovic.fon.mywebproject.dto.EmployeeDto;
import sasa.popovic.fon.mywebproject.repository.EmployeeRepository;
import sasa.popovic.fon.mywebproject.service.EmployeeService;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {
	@Autowired
	private EmployeeRepository employeeRepository;
	@Autowired
	private EmployeeConverter employeeConverter;

	public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
		this.employeeRepository = employeeRepository;
	}

	@Override
	public EmployeeDto findByUsername(String username) {
		List<Employee> employees = employeeRepository.findByUsername(username);
		if (employees.size() != 0)
			return employeeConverter.entityToDto(employees.get(0));
		else
			return null;
	}

}
