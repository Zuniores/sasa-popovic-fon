package sasa.popovic.fon.mywebproject.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sasa.popovic.fon.mywebproject.converter.ExamConverter;
import sasa.popovic.fon.mywebproject.domain.Exam;
import sasa.popovic.fon.mywebproject.domain.Professor;
import sasa.popovic.fon.mywebproject.domain.Subject;
import sasa.popovic.fon.mywebproject.dto.ExamDto;
import sasa.popovic.fon.mywebproject.repository.ExamRepository;
import sasa.popovic.fon.mywebproject.repository.ProfessorRepository;
import sasa.popovic.fon.mywebproject.repository.SubjectRepository;
import sasa.popovic.fon.mywebproject.service.ExamService;

@Service
@Transactional
public class ExamSeviceImpl implements ExamService {

	@Autowired
	private ExamRepository examRepository;
	@Autowired
	private ProfessorRepository professorRepository;
	@Autowired
	private SubjectRepository subjectRepository;
	@Autowired
	private ExamConverter examConverter;

	@Override
	public void save(ExamDto examDto) {
		Exam exam;
		if (examDto.getId() == null) {
			exam = examConverter.dtoToEntity(examDto);
		} else {
			exam = examConverter.dtoToEntityWithId(examDto);
		}
		Subject subject = subjectRepository.findById(examDto.getSubjectDto().getId()).get();
		System.out.println("subject.id: " + subject.getId());
		exam.setSubject(subject);////////

		Professor professor = professorRepository.findById(examDto.getProfessorDto().getId()).get();
		exam.setProfessor(professor);//////////

		examRepository.save(exam);
	}

	@Override
	public ExamDto findById(Long id) throws Exception {
		Optional<Exam> exam = examRepository.findById(id);
		if (exam.isPresent()) {
			return examConverter.entityToDtoWithId(exam.get());
		}
		throw new Exception("Professor does not exist");
	}

	@Override
	public List<ExamDto> getAll() {
		List<ExamDto> examDtos = new ArrayList<ExamDto>();
		List<Exam> exams = examRepository.findAll();
		for (Exam exam : exams) {
			examDtos.add(examConverter.entityToDtoWithId(exam));
		}
		return examDtos;
	}

//	public long betweenDates(Date firstDate, Date secondDate)
//	{
//	    return ChronoUnit.DAYS.between(firstDate.toInstant(), secondDate.toInstant());
//	}

	@Override
	public boolean checkDatesCondition(ExamDto examDto) {

		List<ExamDto> examDtos = getAll();
		for (ExamDto exam : examDtos) {
			if (exam.getProfessorDto().equals(examDto.getProfessorDto())
					&& exam.getSubjectDto().equals(examDto.getSubjectDto())) {
				// we are adding one day to date of the exam from the list
				// because we want to give it the next day effect along with after method
				Date dt = exam.getDate();
				Calendar c = Calendar.getInstance();
				c.setTime(dt);
				c.add(Calendar.DATE, 1);
				dt = c.getTime();
				// if date from list is until yesterday and date from the list is less then
				// defined
				if (dt.after(exam.getDate()) && examDto.getDate().after(exam.getDate())) {
					return true;
				} else {
					return false;
				}
			}
		}
		return true;
	}

}
