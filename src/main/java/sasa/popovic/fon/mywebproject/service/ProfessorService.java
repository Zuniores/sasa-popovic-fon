package sasa.popovic.fon.mywebproject.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sasa.popovic.fon.mywebproject.dto.ProfessorDto;
import sasa.popovic.fon.mywebproject.exception.ProfessorEmailValidationException;

public interface ProfessorService {
	void save(ProfessorDto professorDto) throws ProfessorEmailValidationException;
	ProfessorDto findById(Long id) throws Exception;
	ProfessorDto findByEmail(String email) throws ProfessorEmailValidationException;
	List<ProfessorDto> getAll();
	void checkEmail(String email) throws ProfessorEmailValidationException;
}
