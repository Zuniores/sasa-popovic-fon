package sasa.popovic.fon.mywebproject.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sasa.popovic.fon.mywebproject.converter.StudentConverter;
import sasa.popovic.fon.mywebproject.domain.City;
import sasa.popovic.fon.mywebproject.domain.Student;
import sasa.popovic.fon.mywebproject.dto.StudentDto;
import sasa.popovic.fon.mywebproject.repository.CityRepository;
import sasa.popovic.fon.mywebproject.repository.StudentRepository;
import sasa.popovic.fon.mywebproject.service.StudentService;

@Service
@Transactional
public class StudentServiceImpl implements StudentService {
	@Autowired
	private StudentRepository studentRepository;
	@Autowired
	private CityRepository cityRepository;
	@Autowired
	private StudentConverter studentConverter;

	public StudentServiceImpl(StudentRepository studentRepository) {
		this.studentRepository = studentRepository;
	}

	@Override
	public void save(StudentDto studentDto) {
		Student student;
		if (studentDto.getId() == null) {
			student = studentConverter.dtoToEntity(studentDto);
		} else {
			student = studentConverter.dtoToEntityWithId(studentDto);
		}
		City city = cityRepository.findById(studentDto.getCityDto().getId()).get();
		student.setCity(city);
		studentRepository.save(student);

	}

	@Override
	public StudentDto findById(Long id) throws Exception {
		Optional<Student> student = studentRepository.findById(id);
		if (student.isPresent()) {
			return studentConverter.entityToDtoWithId(student.get());
		}
		throw new Exception("Student does not exist");
	}

	@Override
	public List<StudentDto> getAll() {
		List<Student> students = studentRepository.findAll();
		List<StudentDto> studentDtos = new ArrayList<StudentDto>();
		for (Student student : students) {
			studentDtos.add(studentConverter.entityToDtoWithId(student));
		}
		return studentDtos;
	}

	@Override
	public StudentDto findByindexNumber(String indexNumber) {
		List<Student> students = studentRepository.findByindexNumber(indexNumber);
		if (students.size() != 0) {
			return studentConverter.entityToDtoWithId(students.get(0));
		} else
			return null;
	}

	@Override
	public StudentDto findByEmail(String email) {
		List<Student> students = studentRepository.findByEmail(email);
		if (students.size() != 0) {
			return studentConverter.entityToDtoWithId(students.get(0));
		} else
			return null;
	}

}
