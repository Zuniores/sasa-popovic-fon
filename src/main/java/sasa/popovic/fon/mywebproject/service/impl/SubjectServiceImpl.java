package sasa.popovic.fon.mywebproject.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sasa.popovic.fon.mywebproject.converter.SubjectConverter;
import sasa.popovic.fon.mywebproject.domain.Subject;
import sasa.popovic.fon.mywebproject.dto.SubjectDto;
import sasa.popovic.fon.mywebproject.repository.SubjectRepository;
import sasa.popovic.fon.mywebproject.service.SubjectService;

@Service
@Transactional
public class SubjectServiceImpl implements SubjectService {
	@Autowired
	private SubjectRepository subjectRepository;
	@Autowired
	private SubjectConverter subjectConverter;

	public SubjectServiceImpl(SubjectRepository subjectRepository, SubjectConverter subjectConverter) {
		this.subjectRepository = subjectRepository;
		this.subjectConverter = subjectConverter;
	}

	@Override
	public void save(SubjectDto subjectDto) {
		if (subjectDto.getId() == null) {
			subjectRepository.save(subjectConverter.dtoToEntity(subjectDto));
		} else {
			subjectRepository.save(subjectConverter.dtoToEntityWithId(subjectDto));
		}

	}

	@Override
	public SubjectDto findById(Long id) throws Exception {
		Optional<Subject> subject = subjectRepository.findById(id);
		if (subject.isPresent()) {
			return subjectConverter.entityToDtoWithId(subject.get());
		} else {
			throw new Exception("Student does not exist");
		}

	}

	@Override
	public List<SubjectDto> getAll() {
		List<SubjectDto> subjectDtos = new ArrayList<SubjectDto>();
		List<Subject> subjects = subjectRepository.findAll();
		for (Subject subject : subjects) {
			subjectDtos.add(subjectConverter.entityToDtoWithId(subject));
		}
		return subjectDtos;
	}

}
