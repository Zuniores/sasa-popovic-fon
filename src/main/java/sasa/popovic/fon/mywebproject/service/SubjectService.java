package sasa.popovic.fon.mywebproject.service;

import java.util.List;

import sasa.popovic.fon.mywebproject.dto.SubjectDto;

public interface SubjectService {
	void save(SubjectDto subjectDto);
	SubjectDto findById(Long id) throws Exception;
	List<SubjectDto> getAll();
}
