package sasa.popovic.fon.mywebproject.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sasa.popovic.fon.mywebproject.dto.EmployeeDto;

public interface EmployeeService {
	EmployeeDto findByUsername(String username);
}
