package sasa.popovic.fon.mywebproject.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sasa.popovic.fon.mywebproject.converter.CityConverter;
import sasa.popovic.fon.mywebproject.domain.City;
import sasa.popovic.fon.mywebproject.dto.CityDto;
import sasa.popovic.fon.mywebproject.repository.CityRepository;
import sasa.popovic.fon.mywebproject.service.CityService;

@Service
@Transactional
public class CityServiceImpl implements CityService {
	@Autowired
	private CityRepository cityRepository;
	@Autowired
	private CityConverter cityConverter;

	public CityServiceImpl(CityRepository cityRepository) {
		this.cityRepository = cityRepository;
	}

	@Override
	public void save(CityDto cityDto) {
		if (cityDto == null) {
			cityRepository.save(cityConverter.dtoToEntity(cityDto));
		} else {
			cityRepository.save(cityConverter.dtoToEntityWithId(cityDto));
		}

	}

	@Override
	public CityDto findById(Long id) throws Exception {
		Optional<City> city = cityRepository.findById(id);
		if (city.isPresent()) {
			return cityConverter.entityToDtoWithId(city.get());
		}
		throw new Exception("City does not exist");
	}

	@Override
	public CityDto findByNumber(Long number) {
		List<City> cities = cityRepository.findByNumber(number);
		if (cities.size() != 0)
			return cityConverter.entityToDtoWithId(cities.get(0));
		else
			return null;
	}

	@Override
	public List<CityDto> getAll() {
		List<City> cities = cityRepository.findAll();
		List<CityDto> cityDtos = new ArrayList<CityDto>();
		for (City city : cities) {
			cityDtos.add(cityConverter.entityToDtoWithId(city));
		}
		return cityDtos;
	}

}
