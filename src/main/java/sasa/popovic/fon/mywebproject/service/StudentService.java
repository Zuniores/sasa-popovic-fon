package sasa.popovic.fon.mywebproject.service;

import java.util.List;

import sasa.popovic.fon.mywebproject.dto.StudentDto;

public interface StudentService {
	void save(StudentDto studentDto);
	StudentDto findById(Long id) throws Exception;
	List<StudentDto> getAll();
	StudentDto findByindexNumber(String indexNumber);
	StudentDto findByEmail(String email);
}
