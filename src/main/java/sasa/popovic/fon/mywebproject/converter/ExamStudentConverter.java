package sasa.popovic.fon.mywebproject.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import sasa.popovic.fon.mywebproject.domain.ExamStudent;
import sasa.popovic.fon.mywebproject.dto.ExamStudentDto;

@Component
public class ExamStudentConverter {
	
	@Autowired
	private StudentConverter studentConverter;
	@Autowired
	private ExamConverter examConverter;
	
	public ExamStudentDto entityToDto(ExamStudent examStudent) {
		if(examStudent == null)
			return null;
		else
			return new ExamStudentDto(
					studentConverter.entityToDtoWithId(examStudent.getStudent()),
					examConverter.entityToDtoWithId(examStudent.getExam()),
					examStudent.getEnrollDate()
					);
	}
	
	public ExamStudent dtoToEntity(ExamStudentDto examStudentDto) {
		if(examStudentDto == null)
			return null;
		else
			return new ExamStudent(
					studentConverter.dtoToEntityWithId(examStudentDto.getStudentDto()),
					examConverter.dtoToEntityWithId(examStudentDto.getExamDto()),
					examStudentDto.getEnrollDate()
					);
	}
	
	public ExamStudentDto entityToDtoWithId(ExamStudent examStudent) {
		if(examStudent == null)
			return null;
		else
			return new ExamStudentDto(
					examStudent.getId(),
					studentConverter.entityToDtoWithId(examStudent.getStudent()),
					examConverter.entityToDtoWithId(examStudent.getExam()),
					examStudent.getEnrollDate()
					);
	}
	
	public ExamStudent dtoToEntityWithId(ExamStudentDto examStudentDto) {
		if(examStudentDto == null)
			return null;
		else
			return new ExamStudent(
					examStudentDto.getId(),
					studentConverter.dtoToEntityWithId(examStudentDto.getStudentDto()),
					examConverter.dtoToEntityWithId(examStudentDto.getExamDto()),
					examStudentDto.getEnrollDate()
					);
	}
	
	public List<ExamStudentDto> listEntityToDto(List<ExamStudent> examStudents) {
		if(examStudents == null)
			return null;
		else {
			List<ExamStudentDto> examStudentDtos = new ArrayList<ExamStudentDto>();
			for (ExamStudent examStudent : examStudents) {
				examStudentDtos.add(new ExamStudentDto(
						studentConverter.entityToDtoWithId(examStudent.getStudent()),
						examConverter.entityToDtoWithId(examStudent.getExam()),
						examStudent.getEnrollDate()
						));
			}
			return examStudentDtos;
		}
	}
	
	public List<ExamStudent> listDtoToEntity(List<ExamStudentDto> examStudentDtos) {
		if(examStudentDtos == null)
			return null;
		else {
			List<ExamStudent> examStudents = new ArrayList<ExamStudent>();
			for (ExamStudentDto examStudentDto : examStudentDtos) {
				examStudents.add(new ExamStudent(
						studentConverter.dtoToEntityWithId(examStudentDto.getStudentDto()),
						examConverter.dtoToEntityWithId(examStudentDto.getExamDto()),
						examStudentDto.getEnrollDate()
						));
			}
			return examStudents;
		}
	}
	
	public List<ExamStudentDto> listEntityToDtoWithId(List<ExamStudent> examStudents) {
		if(examStudents == null)
			return null;
		else {
			List<ExamStudentDto> examStudentDtos = new ArrayList<ExamStudentDto>();
			for (ExamStudent examStudent : examStudents) {
				examStudentDtos.add(new ExamStudentDto(
						examStudent.getId(),
						studentConverter.entityToDtoWithId(examStudent.getStudent()),
						examConverter.entityToDtoWithId(examStudent.getExam()),
						examStudent.getEnrollDate()
						));
			}
			return examStudentDtos;
		}
	}
	
	public List<ExamStudent> listDtoToEntityWithId(List<ExamStudentDto> examStudentDtos) {
		if(examStudentDtos == null)
			return null;
		else {
			List<ExamStudent> examStudents = new ArrayList<ExamStudent>();
			for (ExamStudentDto examStudentDto : examStudentDtos) {
				examStudents.add(new ExamStudent(
						examStudentDto.getId(),
						studentConverter.dtoToEntityWithId(examStudentDto.getStudentDto()),
						examConverter.dtoToEntityWithId(examStudentDto.getExamDto()),
						examStudentDto.getEnrollDate()
						));
			}
			return examStudents;
		}
	}
}
