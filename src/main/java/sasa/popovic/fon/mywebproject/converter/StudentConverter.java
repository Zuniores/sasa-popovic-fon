package sasa.popovic.fon.mywebproject.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import sasa.popovic.fon.mywebproject.domain.Student;
import sasa.popovic.fon.mywebproject.dto.StudentDto;


@Component
public class StudentConverter {
	@Autowired
	private CityConverter cityConverter;
	
	public Student dtoToEntity(StudentDto studentDto) {
		return new Student(
				studentDto.getIndexNumber(),
				studentDto.getFirstname(),
				studentDto.getLastname(),
				studentDto.getEmail(),
				studentDto.getAddress(),
				cityConverter.dtoToEntityWithId(studentDto.getCityDto()),
				studentDto.getPhone(),
				studentDto.getCurrentYearOfStudy()
				);
	}
	
	public StudentDto entityToDto(Student student) {
		return new StudentDto(
				student.getIndexNumber(),
				student.getFirstname(),
				student.getLastname(),
				student.getEmail(),
				student.getAddress(),
				cityConverter.entityToDtoWithId(student.getCity()),
				student.getPhone(),
				student.getCurrentYearOfStudy()
				);
	}
	
	public Student dtoToEntityWithId(StudentDto studentDto) {
		return new Student(
				studentDto.getId(),
				studentDto.getIndexNumber(),
				studentDto.getFirstname(),
				studentDto.getLastname(),
				studentDto.getEmail(),
				studentDto.getAddress(),
				cityConverter.dtoToEntityWithId(studentDto.getCityDto()),
				studentDto.getPhone(),
				studentDto.getCurrentYearOfStudy()
				);
	}
	
	public StudentDto entityToDtoWithId(Student student) {
		return new StudentDto(
				student.getId(),
				student.getIndexNumber(),
				student.getFirstname(),
				student.getLastname(),
				student.getEmail(),
				student.getAddress(),
				cityConverter.entityToDtoWithId(student.getCity()),
				student.getPhone(),
				student.getCurrentYearOfStudy()
				);
	}
}
