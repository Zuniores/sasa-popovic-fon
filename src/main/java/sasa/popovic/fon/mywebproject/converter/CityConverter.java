package sasa.popovic.fon.mywebproject.converter;

import org.springframework.stereotype.Component;

import sasa.popovic.fon.mywebproject.domain.City;
import sasa.popovic.fon.mywebproject.dto.CityDto;


@Component
public class CityConverter {
	
	public City dtoToEntity(CityDto cityDto) {
		if(cityDto == null)
			return null;
		else
			return new City(
					cityDto.getNumber(), 
					cityDto.getName());
	}
	
	public CityDto entityToDto(City city) {
		if(city == null)
			return null;
		else
			return new CityDto(
					city.getNumber(), 
					city.getName());
	}
	
	public City dtoToEntityWithId(CityDto cityDto) {
		if(cityDto == null)
			return null;
		else
			return new City(
					cityDto.getId(),
					cityDto.getNumber(), 
					cityDto.getName());
	}
	
	public CityDto entityToDtoWithId(City city) {
		if(city == null)
			return null;
		else
			return new CityDto(
					city.getId(),
					city.getNumber(), 
					city.getName());
	}
}
