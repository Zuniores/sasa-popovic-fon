package sasa.popovic.fon.mywebproject.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import sasa.popovic.fon.mywebproject.domain.Exam;
import sasa.popovic.fon.mywebproject.dto.ExamDto;

@Component
public class ExamConverter {

	@Autowired
	private ProfessorConverter professorConverter;
	@Autowired
	private SubjectConverter subjectConverter;

	public Exam dtoToEntity(ExamDto examDto) {
		if (examDto == null)
			return null;
		else
			return new Exam(
					professorConverter.dtoToEntityWithId(examDto.getProfessorDto()),
					subjectConverter.dtoToEntityWithId(examDto.getSubjectDto()),
					examDto.getDate());
	}

	public ExamDto entityToDto(Exam exam) {
		if (exam == null)
			return null;
		else
			return new ExamDto(
					professorConverter.entityToDtoWithId(exam.getProfessor()),
					subjectConverter.entityToDtoWithId(exam.getSubject()),
					exam.getDate()
					);

	}

	public Exam dtoToEntityWithId(ExamDto examDto) {
		if (examDto == null)
			return null;
		else
			return new Exam(
					examDto.getId(),
					professorConverter.dtoToEntityWithId(examDto.getProfessorDto()),
					subjectConverter.dtoToEntityWithId(examDto.getSubjectDto()),
					examDto.getDate());
	}

	public ExamDto entityToDtoWithId(Exam exam) {
		if (exam == null)
			return null;
		else
			return new ExamDto(
					exam.getId(),
					professorConverter.entityToDtoWithId(exam.getProfessor()),
					subjectConverter.entityToDtoWithId(exam.getSubject()),
					exam.getDate()
					);

	}
	
	public List<Exam> listDtoToEntity(List<ExamDto> examDtos){
		if(examDtos == null)
			return null;
		else {
			List<Exam> exams = new ArrayList<Exam>();
			for (ExamDto examDto : examDtos) {
				exams.add(dtoToEntityWithId(examDto));
			}
			return exams;
		}
	}
	
	public List<ExamDto> listEntityToDto(List<Exam> exams){
		if(exams == null)
			return null;
		else {
			List<ExamDto> examDtos = new ArrayList<ExamDto>();
			for (Exam exam : exams) {
				examDtos.add(entityToDtoWithId(exam));
			}
			return examDtos;
		}
	}

}
