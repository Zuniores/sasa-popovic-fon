package sasa.popovic.fon.mywebproject.converter;

import org.springframework.stereotype.Component;

import sasa.popovic.fon.mywebproject.domain.Employee;
import sasa.popovic.fon.mywebproject.dto.EmployeeDto;

@Component
public class EmployeeConverter {
	
	public Employee dtoToEntity(EmployeeDto employeeDto) {
		return new Employee(
				employeeDto.getUsername(),
				employeeDto.getPassword(),
				employeeDto.getFirstname(),
				employeeDto.getLastname());
	}
	
	public EmployeeDto entityToDto(Employee employee) {
		return new EmployeeDto(
				employee.getUsername(),
				employee.getPassword(),
				employee.getFirstname(),
				employee.getLastname());
	}
}
