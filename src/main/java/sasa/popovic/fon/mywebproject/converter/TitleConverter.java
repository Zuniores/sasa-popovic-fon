package sasa.popovic.fon.mywebproject.converter;

import org.springframework.stereotype.Component;

import sasa.popovic.fon.mywebproject.domain.Title;
import sasa.popovic.fon.mywebproject.dto.TitleDto;

@Component
public class TitleConverter {
	
	public Title dtoToEntity(TitleDto titleDto) {
		return new Title(titleDto.getName());
	}
	
	public TitleDto entityToDto(Title title) {
		return new TitleDto(title.getName());
	}
	
	public Title dtoToEntityWithId(TitleDto titleDto) {
		return new Title(titleDto.getId(), titleDto.getName());
	}
	
	public TitleDto entityToDtoWithId(Title title) {
		return new TitleDto(title.getId(), title.getName());
	}
}
