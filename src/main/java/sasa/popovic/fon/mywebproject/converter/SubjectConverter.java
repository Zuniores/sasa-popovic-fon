package sasa.popovic.fon.mywebproject.converter;

import org.springframework.stereotype.Component;

import sasa.popovic.fon.mywebproject.domain.Subject;
import sasa.popovic.fon.mywebproject.dto.SubjectDto;

@Component
public class SubjectConverter {
	
	public Subject dtoToEntity(SubjectDto subjectDto) {
		return new Subject(
				subjectDto.getName(),
				subjectDto.getDescription(),
				subjectDto.getYearOfStudy(),
				subjectDto.getSemester()
				);
	}
	
	public SubjectDto entityToDto(Subject subject) {
		return new SubjectDto(
				subject.getName(),
				subject.getDescription(),
				subject.getYearOfStudy(),
				subject.getSemester()
				);
	}
	
	public Subject dtoToEntityWithId(SubjectDto subjectDto) {
		return new Subject(
				subjectDto.getId(),
				subjectDto.getName(),
				subjectDto.getDescription(),
				subjectDto.getYearOfStudy(),
				subjectDto.getSemester()
				);
	}
	
	public SubjectDto entityToDtoWithId(Subject subject) {
		return new SubjectDto(
				subject.getId(),
				subject.getName(),
				subject.getDescription(),
				subject.getYearOfStudy(),
				subject.getSemester()
				);
	}
}
