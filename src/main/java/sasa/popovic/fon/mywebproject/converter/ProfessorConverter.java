package sasa.popovic.fon.mywebproject.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import sasa.popovic.fon.mywebproject.domain.Professor;
import sasa.popovic.fon.mywebproject.dto.ProfessorDto;


@Component
public class ProfessorConverter {
	
	@Autowired
	private CityConverter cityConverter;
	@Autowired
	private TitleConverter titleConverter;
	@Autowired
	private ExamConverter examConverter;
	
	public ProfessorDto entityToDto(Professor professor) {
		return new ProfessorDto(
				professor.getFirstname(),
				professor.getLastname(),
				professor.getEmail(),
				professor.getAddress(),
				cityConverter.entityToDtoWithId(professor.getCity()),
				professor.getPhone(),
				professor.getReelectionDate(),
				titleConverter.entityToDtoWithId(professor.getTitle()),
				null
				);
	}
	
	public Professor dtoToEntity(ProfessorDto professorDto) {
		return new Professor(
				professorDto.getFirstname(),
				professorDto.getLastname(),
				professorDto.getEmail(),
				professorDto.getAddress(),
				cityConverter.dtoToEntityWithId(professorDto.getCityDto()),
				professorDto.getPhone(),
				professorDto.getReelectionDate(),
				titleConverter.dtoToEntityWithId(professorDto.getTitleDto()),
				null
				);
	}
	
	public ProfessorDto entityToDtoWithId(Professor professor) {
		return new ProfessorDto(
				professor.getId(),
				professor.getFirstname(),
				professor.getLastname(),
				professor.getEmail(),
				professor.getAddress(),
				cityConverter.entityToDtoWithId(professor.getCity()),
				professor.getPhone(),
				professor.getReelectionDate(),
				titleConverter.entityToDtoWithId(professor.getTitle()),
				null
				);
	}
	
	public Professor dtoToEntityWithId(ProfessorDto professorDto) {
		return new Professor(
				professorDto.getId(),
				professorDto.getFirstname(),
				professorDto.getLastname(),
				professorDto.getEmail(),
				professorDto.getAddress(),
				cityConverter.dtoToEntityWithId(professorDto.getCityDto()),
				professorDto.getPhone(),
				professorDto.getReelectionDate(),
				titleConverter.dtoToEntityWithId(professorDto.getTitleDto()),
				null
				);
	}
}
