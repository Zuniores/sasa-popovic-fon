package sasa.popovic.fon.mywebproject.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "EXAM_STUDENT", uniqueConstraints = { @UniqueConstraint(columnNames = { "STUDENT_ID", "EXAM_ID" }) })
public class ExamStudent {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "EXAM_STUDENT_ID")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "STUDENT_ID", referencedColumnName = "STUDENT_ID") //
	private Student student;

	@ManyToOne
	@JoinColumn(name = "EXAM_ID", referencedColumnName = "EXAM_ID") //
	private Exam exam;

	@Temporal(TemporalType.DATE)
	private Date enrollDate;

	public ExamStudent() {

	}

	public ExamStudent(Student student, Exam exam, Date enrollDate) {
		this.student = student;
		this.exam = exam;
		this.enrollDate = enrollDate;
	}

	public ExamStudent(Long id, Student student, Exam exam, Date enrollDate) {
		this.id = id;
		this.student = student;
		this.exam = exam;
		this.enrollDate = enrollDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Exam getExam() {
		return exam;
	}

	public void setExam(Exam exam) {
		this.exam = exam;
	}

	public Date getEnrollDate() {
		return enrollDate;
	}

	public void setEnrollDate(Date enrollDate) {
		this.enrollDate = enrollDate;
	}

}
