package sasa.popovic.fon.mywebproject.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "EXAM")
//uniqueConstraints = { @UniqueConstraint(columnNames = { "PROFESSOR_ID", "SUBJECT_ID", "DATE" }) }
public class Exam {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "EXAM_ID")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "PROFESSOR_ID", referencedColumnName = "PROFESSOR_ID")
	private Professor professor;

	@ManyToOne
	@JoinColumn(name = "SUBJECT_ID", referencedColumnName = "SUBJECT_ID")
	private Subject subject;

	@Temporal(TemporalType.DATE)
	@Column(name = "DATE")
	private Date date;

	public Exam() {

	}

	public Exam(Professor professor, Subject subject, Date date) {
		this.professor = professor;
		this.subject = subject;
		this.date = date;
	}

	public Exam(Long id, Professor professor, Subject subject, Date date) {
		super();
		this.id = id;
		this.professor = professor;
		this.subject = subject;
		this.date = date;
	}

	public Exam(Professor professor, Subject subject, List<ExamStudent> examStudents, Date date) {
		super();
		this.professor = professor;
		this.subject = subject;
		this.date = date;
	}

	public Exam(Long id, Professor professor, Subject subject, List<ExamStudent> examStudents, Date date) {
		super();
		this.id = id;
		this.professor = professor;
		this.subject = subject;
		this.date = date;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Professor getProfessor() {
		return professor;
	}

	public void setProfessor(Professor professor) {
		this.professor = professor;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
