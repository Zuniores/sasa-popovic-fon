package sasa.popovic.fon.mywebproject.domain;

public enum Semester {
	Summer, Winter
}
