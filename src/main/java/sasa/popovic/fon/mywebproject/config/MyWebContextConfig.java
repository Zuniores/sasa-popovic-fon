package sasa.popovic.fon.mywebproject.config;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesViewResolver;

import sasa.popovic.fon.mywebproject.formatter.CityDtoFormatter;
import sasa.popovic.fon.mywebproject.formatter.DateFormatter;
import sasa.popovic.fon.mywebproject.formatter.EmployeeDtoFormatter;
import sasa.popovic.fon.mywebproject.formatter.ExamDtoFormatter;
import sasa.popovic.fon.mywebproject.formatter.ExamStudentDtoFormatter;
import sasa.popovic.fon.mywebproject.formatter.LongFormatter;
import sasa.popovic.fon.mywebproject.formatter.ProfessorDtoFormatter;
import sasa.popovic.fon.mywebproject.formatter.StudentDtoFormatter;
import sasa.popovic.fon.mywebproject.formatter.SubjectDtoFormatter;
import sasa.popovic.fon.mywebproject.formatter.TitleDtoFormatter;
import sasa.popovic.fon.mywebproject.service.CityService;
import sasa.popovic.fon.mywebproject.service.EmployeeService;
import sasa.popovic.fon.mywebproject.service.ExamService;
import sasa.popovic.fon.mywebproject.service.ExamStudentService;
import sasa.popovic.fon.mywebproject.service.ProfessorService;
import sasa.popovic.fon.mywebproject.service.StudentService;
import sasa.popovic.fon.mywebproject.service.SubjectService;
import sasa.popovic.fon.mywebproject.service.TitleService;


@EnableWebMvc
@ComponentScan(basePackages = {
		"sasa.popovic.fon.mywebproject.controller",
		"sasa.popovic.fon.mywebproject.exception"
})
@Configuration
@Import(MyDatabaseConfig.class)
public class MyWebContextConfig implements WebMvcConfigurer{
	@Autowired
	private EmployeeService employeeService;
	@Autowired
	private CityService cityService;
	@Autowired
	private StudentService studentService;
	@Autowired
	private TitleService titleService;
	@Autowired
	private ProfessorService professorService;
	@Autowired
	private SubjectService subjectService;
	@Autowired
	private ExamService examService;
	@Autowired
	private ExamStudentService examStudentService;
	
	@Autowired
	public MyWebContextConfig(
			EmployeeService employeeService, 
			CityService cityService,
			StudentService studentService,
			TitleService titleService,
			ProfessorService professorService,
			SubjectService subjectService,
			ExamService examService,
			ExamStudentService examStudentService) {
		System.out.println("=================================================================");
		System.out.println("==================== MyWebContextConfig =========================");
		System.out.println("=================================================================");
		this.employeeService = employeeService;
		this.cityService = cityService;
		this.studentService = studentService;
		this.titleService = titleService;
	}
	
	//view resolvers
	@Bean
	public ViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/WEB-INF/pages/");
		viewResolver.setSuffix(".jsp");
		viewResolver.setOrder(1);
		return viewResolver;
	}
	
	//TilesViewResolver
	@Bean
	public ViewResolver tilesViewResolver() {
		TilesViewResolver tilesViewResolver = new TilesViewResolver();
		tilesViewResolver.setOrder(0);
		return tilesViewResolver;
	}
	
	//Konfigurisanje TilesViewResolvera
	@Bean
	public TilesConfigurer tilesCongigurer() {
		TilesConfigurer tilesConfigurer = new TilesConfigurer();
		tilesConfigurer.setDefinitions(
				new String[] {"/WEB-INF/views/tiles/tiles.xml"}
		);
		return tilesConfigurer;
	}
	
	@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
	}
	
	
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/").setViewName("forward:authentication/login");
	}
	
	@Override
	public void addFormatters(FormatterRegistry registry) {
		registry.addFormatter(new EmployeeDtoFormatter(employeeService));
		registry.addFormatter(new CityDtoFormatter(cityService));
		registry.addFormatter(new StudentDtoFormatter(studentService));
		registry.addFormatter(new TitleDtoFormatter(titleService));
		registry.addFormatter(new ProfessorDtoFormatter(professorService));
		registry.addFormatter(new SubjectDtoFormatter(subjectService));
		registry.addFormatter(new ExamDtoFormatter(examService));
		registry.addFormatter(new ExamStudentDtoFormatter(examStudentService));
		registry.addFormatter(new LongFormatter());
		registry.addFormatter(new DateFormatter());
	}
	
//	@Override
//    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        registry
//          .addResourceHandler("/webjars/**")
//          .addResourceLocations("/webjars/");
//    }
	
}
