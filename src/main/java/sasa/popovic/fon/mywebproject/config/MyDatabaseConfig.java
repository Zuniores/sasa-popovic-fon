package sasa.popovic.fon.mywebproject.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories(
		basePackages = "sasa.popovic.fon.mywebproject.repository"
)
@ComponentScan(basePackages = {
		"sasa.popovic.fon.mywebproject.service",
		"sasa.popovic.fon.mywebproject.converter"
})
@PropertySources({
	@PropertySource("classpath:database.conf")
})
@EnableTransactionManagement
public class MyDatabaseConfig {
	@Autowired
	private Environment env;
	
	@Bean
	public DataSource datasource() {
		DriverManagerDataSource datasource = new DriverManagerDataSource();
		//string username is stucked as "sasa.popovic", so dont use it
		datasource.setDriverClassName(env.getProperty("driverClassName"));
		datasource.setUrl(env.getProperty("url"));
		datasource.setUsername(env.getProperty("probaUsername"));
		datasource.setPassword(env.getProperty("password"));
//		
//		datasource.setDriverClassName("com.mysql.cj.jdbc.Driver");
//		datasource.setUrl("jdbc:mysql://localhost:3306/sasa_popovic_fon");
//		datasource.setUsername("root");
//		datasource.setPassword("12345");
		return datasource;
	}
	
	@Bean
	JdbcTemplate jdbcTemplate() {
		return new JdbcTemplate(datasource());
	}

	@Bean(name = "entityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(datasource());
		em.setPackagesToScan(new String[] {"sasa.popovic.fon.mywebproject.domain"});
		HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
		jpaVendorAdapter.setShowSql(true);
//		jpaVendorAdapter.setGenerateDdl(true); //Auto creating scheme when true
//		jpaVendorAdapter.setDatabase(Database.MYSQL);//Database type
		em.setJpaVendorAdapter(jpaVendorAdapter);
		
		em.setJpaProperties(getAdditionPropertiies());
		return em;
	}

	private Properties getAdditionPropertiies() {
		Properties properties = new Properties();
//		properties.setProperty("hibernate.show_sql", "true");
//		properties.setProperty("hibernate.hbm2ddl.auto", "update");
//		properties.setProperty("hibernate.format_sql", "true");
//		properties.setProperty("hibernate.dialect","org.hibernate.dialect.MySQL5Dialect");
		return properties;
	}
	
	@Bean
	public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(emf);
		return transactionManager;
	}
}
