package sasa.popovic.fon.mywebproject.exception;

public class ProfessorEmailValidationException extends Exception{
	private static final long serialVersionUID = 2650597008240739471L;

	public ProfessorEmailValidationException(String message) {
		super(message);
	}
}
